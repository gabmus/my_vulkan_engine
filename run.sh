#!/bin/bash

set -e

if [[ ! -d build ]]; then
    mkdir build
    pushd build
    meson ..
    meson configure -Dprefix=$PWD/mprefix
    popd
fi

ninja -C build
ninja -C build install
./build/mprefix/bin/MyFirstVulkanApp
