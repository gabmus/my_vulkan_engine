#pragma once
#include <cstdint>

#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"
#include "engine_model.hpp"
#include <memory>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/transform.hpp>

struct TransformComponentQuat {
    glm::vec3 translation{};
    glm::vec3 scale{1.f, 1.f, 1.f};
    glm::quat rotation{1.f, 0.f, 0.f, 0.f};

    // 3 spacial dimensions + 1 for homogeneous coordinates
    glm::mat4 mat4() {
        return glm::translate(translation)
            * glm::mat4_cast(rotation)
            * glm::scale(scale);
    }
};

/**
* Represents an object in the scene. This implementation uses quaternions for
* rotation, so that they can be easily manipulated by the physics engine.
*
* EngineObjectQuats have a unique id, a name and an EngineModel that represents
* their shape.
*
* EngineObjectQuats can have a limited lifetime, in which case they can "die",
* meaning they will be removed from the scene once their lifetime expires.
*/
class EngineObjectQuat {
public:
    using id_t = uint32_t;

    std::shared_ptr<EngineModel> model{};
    VkDescriptorSet* texture_desc_set;
    glm::vec3 color{};
    TransformComponentQuat transform{};
    btRigidBody* rigid_body = nullptr;
    std::string name = "";
    bool can_die = false;
    float lifetime = 0;
    bool to_kill = false;

    static EngineObjectQuat create_engine_object() {
        static id_t current_id = 0;
        return EngineObjectQuat{current_id++};
    }

    glm::vec3 get_bounding_box_size() {
        // if model is unset, assume it's the character/camera object
        // and give some default measurements for a capsule shape
        if (!model) return {
            0.5, 0.7, 0.5
        };
        // box size is in half extents, hence the /2
        auto res =  (transform.scale * model->get_bounding_box_size())/2.0f;
        res.x = fmax(0.01, res.x);
        res.y = fmax(0.01, res.y);
        res.z = fmax(0.01, res.z);
        return res;
    }

    const id_t getId() { return id; }

    /**
    * Sets the rigid body transofrm to match the one defined by the object
    */
    void set_rigid_body_transform() {
        if (rigid_body) {
            btQuaternion bt_rotation;
            auto bt_transform = btTransform(
                btQuaternion(
                    transform.rotation.x,
                    transform.rotation.y,
                    transform.rotation.z,
                    transform.rotation.w
                ),
                btVector3(
                    transform.translation.x,
                    transform.translation.y,
                    transform.translation.z
                )
            );
            rigid_body->getMotionState()->setWorldTransform(bt_transform);
            rigid_body->setWorldTransform(bt_transform);
            rigid_body->setLinearVelocity(btVector3(0, 0, 0));
            rigid_body->setAngularVelocity(btVector3(0, 0, 0));
            rigid_body->clearForces();
            rigid_body->activate();
        }
    }

    /**
    * Apply an impulsive force on the object's rigid body, propelling it in
    * the given direction
    */
    void apply_force(glm::vec3 direction, float amount) {
        if (!rigid_body) {
            std::cerr <<
                "WARNING: trying to apply force to non-physiscs body named " <<
                name <<
                std::endl;
            return;
        }
        direction = glm::normalize(direction);
        rigid_body->setLinearVelocity(
            btVector3(direction.x, direction.y, direction.z) * amount
        );
        rigid_body->activate();
    }

    glm::vec3 get_direction() {
        // experimental
        return glm::normalize(glm::vec3(
            2 * (
                (transform.rotation.x * transform.rotation.z) +
                (transform.rotation.w * transform.rotation.y)
            ),
            2 * (
                (transform.rotation.y * transform.rotation.z) +
                (transform.rotation.w * transform.rotation.x)
            ),
            1 - (2 * (
                (transform.rotation.x * transform.rotation.x) +
                (transform.rotation.y * transform.rotation.y))
            )
        ));
    }

    EngineObjectQuat(const EngineObjectQuat &) = delete;
    EngineObjectQuat& operator=(const EngineObjectQuat &) = delete;
    EngineObjectQuat(EngineObjectQuat&&) = default;
    EngineObjectQuat& operator=(EngineObjectQuat&&) = default;
protected:
    id_t id;

    EngineObjectQuat(id_t obj_id): id(obj_id) {}
};
