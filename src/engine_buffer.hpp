#pragma once

#include "engine_device.hpp"
#include "libs/vk_mem_alloc.h"

/**
* Abstraction around VkBuffer. It automatically deals with memory alignment
* and can create a single buffer for use with multiple structures
*/
class EngineBuffer {
private:
    static VkDeviceSize getAlignment(VkDeviceSize instanceSize, VkDeviceSize minOffsetAlignment) {
        if (minOffsetAlignment > 0) {
            return (instanceSize + minOffsetAlignment - 1) & ~(minOffsetAlignment - 1);
        }
        return instanceSize;
    }

    EngineDevice& device;
    void* mapped = nullptr;
    VkBuffer buffer = VK_NULL_HANDLE;
    VmaAllocation allocation = VK_NULL_HANDLE;

    VkDeviceSize alignmentSize;
    VkDeviceSize bufferSize;
    uint32_t instanceCount;
    VkDeviceSize instanceSize;
    VkBufferUsageFlags usageFlags;
    VmaMemoryUsage memoryUsage;
    VkMemoryPropertyFlags memoryPropertyFlags;

public:
    EngineBuffer(
            EngineDevice& device,
            VkDeviceSize instanceSize,
            uint32_t instanceCount,
            VkBufferUsageFlags usageFlags,
            VmaMemoryUsage memoryUsage,
            VkMemoryPropertyFlags memoryPropertyFlags,
            VkDeviceSize minOffsetAlignment=1
    ) : device{device}, instanceSize{instanceSize}, instanceCount{instanceCount},
        usageFlags{usageFlags}, memoryUsage{memoryUsage},
        memoryPropertyFlags{memoryPropertyFlags} {
        alignmentSize = getAlignment(instanceSize, minOffsetAlignment);
        bufferSize = alignmentSize * instanceCount;
        device.create_buffer(
            bufferSize, usageFlags, memoryPropertyFlags, memoryUsage, buffer, allocation
        );
    }
    EngineBuffer(
            EngineDevice& device,
            VkDeviceSize instanceSize,
            uint32_t instanceCount,
            VkBufferUsageFlags usageFlags,
            VkMemoryPropertyFlags memoryPropertyFlags,
            VkDeviceSize minOffsetAlignment=1
    ) : device{device}, instanceSize{instanceSize}, instanceCount{instanceCount},
        usageFlags{usageFlags}, memoryPropertyFlags{memoryPropertyFlags} {
        alignmentSize = getAlignment(instanceSize, minOffsetAlignment);
        bufferSize = alignmentSize * instanceCount;
        device.create_buffer(
            bufferSize, usageFlags, memoryPropertyFlags, buffer, allocation
        );
    }

    ~EngineBuffer() {
        unmap();
        device.destroy_buffer(buffer, allocation);
    }

    // RAII
    EngineBuffer(const EngineBuffer&) = delete;
    EngineBuffer& operator=(const EngineBuffer&) = delete;
    
    VkResult map() {
        return vmaMapMemory(device.get_allocator(), allocation, &mapped);
    }
    void unmap() {
        if (mapped) {
            vmaUnmapMemory(device.get_allocator(), allocation);
            mapped = nullptr;
        }
    }

    void writeToBuffer(void* data, VkDeviceSize size=VK_WHOLE_SIZE, VkDeviceSize offset=0) {
        assert(mapped && "Cannot copy to unmapped buffer");
        if (size == VK_WHOLE_SIZE) {
            memcpy(mapped, data, bufferSize);
        }
        else {
            char* mem_offset = (char*)mapped;
            mem_offset += offset;
            memcpy(mem_offset, data, size);
        }
    }
    VkResult flush(VkDeviceSize size=VK_WHOLE_SIZE, VkDeviceSize offset=0) {
        return vmaFlushAllocation(
            device.get_allocator(), allocation, offset, size
        );
    }
    VkResult invalidate(VkDeviceSize size=VK_WHOLE_SIZE, VkDeviceSize offset=0) {
        return vmaInvalidateAllocation(
            device.get_allocator(), allocation, offset, size
        );
    }

    VkDescriptorBufferInfo descriptorInfo(VkDeviceSize size=VK_WHOLE_SIZE, VkDeviceSize offset=0) {
        return VkDescriptorBufferInfo {
            buffer, offset, size
        };
    }

    void writeToIndex(void* data, int index) {
        writeToBuffer(data, instanceSize, index * alignmentSize);
    }
    VkResult flushIndex(int index) {
        return flush(alignmentSize, index * alignmentSize);
    }
    VkDescriptorBufferInfo descriptorInfoForIndex(int index) {
        return descriptorInfo(alignmentSize, index * alignmentSize);
    }
    VkResult invalidateIndex(int index) {
        return invalidate(alignmentSize, index * alignmentSize);
    }
    
    VkBuffer getBuffer() const { return buffer; }
    void* getMappedMemory() const { return mapped; }
    uint32_t getInstanceCount() const { return instanceCount; }
    VkDeviceSize getInstanceSize() const { return instanceSize; }
    VkDeviceSize getAlignmentSize() const { return instanceSize; }
    VkBufferUsageFlags getUsageFlags() const { return usageFlags; }
    VkMemoryPropertyFlags getMemoryPropertyFlags() const { return memoryPropertyFlags; }
    VkDeviceSize getBufferSize() const { return bufferSize; }
};
