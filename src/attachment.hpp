#pragma once

#include "engine_device.hpp"

/**
* Attachment for a render pass. This class is responsible for managing the
* Vulkan image and image view, as well as its allocation and any other
* metadata required to define a render pass attachment. In case the resulting
* image needs to be read, it can also generate an appropriate sampler.
*/
class Attachment {
private:
    EngineDevice& device;

    VkFormat format;
    VkImage image;
    VkImageView image_view;
    VkSampler image_sampler;
    bool image_sampler_inited{false};
    VmaAllocation image_allocation;

    VkAttachmentDescription att_desc{};
    VkAttachmentReference att_ref{};

    VkExtent2D extent;  // width, height

    VkImageUsageFlags usage;
    VkImageAspectFlags aspect_mask;

    VkImageLayout final_layout;
    VkImageLayout att_layout;
    uint32_t att_index;

    bool reduce_size{false};
    uint32_t size_divisor;

    void init() {
        VkImageCreateInfo image_info{};
        image_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        image_info.imageType = VK_IMAGE_TYPE_2D;
        image_info.format = format;
        image_info.extent.width = extent.width / size_divisor;
        image_info.extent.height = extent.height / size_divisor;
        image_info.extent.depth = 1;
        image_info.mipLevels = 1;
        image_info.arrayLayers = 1;
        image_info.samples = VK_SAMPLE_COUNT_1_BIT;
        image_info.tiling = VK_IMAGE_TILING_OPTIMAL;
        image_info.usage = usage;
        device.create_image_with_info(
            image_info,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
            image, image_allocation
        );

        VkImageViewCreateInfo image_view_info{};
        image_view_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        image_view_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
        image_view_info.format = format;
        image_view_info.subresourceRange = {};
        image_view_info.subresourceRange.aspectMask = aspect_mask;
        image_view_info.subresourceRange.baseMipLevel = 0;
        image_view_info.subresourceRange.levelCount = 1;
        image_view_info.subresourceRange.baseArrayLayer = 0;
        image_view_info.subresourceRange.layerCount = 1;
        image_view_info.image = image;
        device.create_image_view_with_info(image_view_info, image_view);

        att_desc.format = format;
        att_desc.samples = VK_SAMPLE_COUNT_1_BIT;
        att_desc.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        att_desc.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        att_desc.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        att_desc.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        att_desc.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        att_desc.finalLayout = final_layout;

        att_ref.attachment = att_index;
        att_ref.layout = att_layout;
    }

    void init_sampler() {
        VkSamplerCreateInfo sampler_info{};
        sampler_info.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
        sampler_info.magFilter = VK_FILTER_LINEAR;
        sampler_info.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
        sampler_info.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        sampler_info.addressModeV = sampler_info.addressModeU;
        sampler_info.addressModeW = sampler_info.addressModeU;
        sampler_info.mipLodBias = 0.0f;
        sampler_info.maxAnisotropy = 1.0f;
        sampler_info.minLod = 0.0f;
        sampler_info.maxLod = 1.0f;
        sampler_info.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
        device.create_sampler_with_info(sampler_info, image_sampler);
        image_sampler_inited = true;
    }

    void cleanup() {
        vkDestroyImageView(device.device(), image_view, nullptr);
        vmaDestroyImage(device.get_allocator(), image, image_allocation);
    }

public:

    /**
    * Initializes the Attachment object, given all the required metadata.
    * Some parameters are set to sane defaults that never need to change
    * so they're not required in the constructor.
    *
    * @param width width of the viewport
    * @param height height of the viewport
    * @param reduce_size some render passes need an attachment half the size
    *        of the regular viewport size. In this case, the full width and
    *        height of the viewport are passed to this constructor, and this
    *        flag is set to true. This makes the code more consistent.
    */
    Attachment(
            EngineDevice& device, VkFormat format, uint32_t width, uint32_t height,
            VkImageUsageFlags usage, VkImageAspectFlags aspect_mask, VkImageLayout final_layout,
            VkImageLayout att_layout, uint32_t att_index, bool reduce_size
    )
        : device{device}, format{format}, extent{VkExtent2D{width, height}},
        usage{usage}, aspect_mask{aspect_mask}, final_layout{final_layout},
        att_layout{att_layout}, att_index{att_index}, reduce_size{reduce_size},
        size_divisor{reduce_size ? 2u : 1u}
    {
        init();
    }

    ~Attachment() {
        cleanup();
        if (image_sampler_inited) {
            vkDestroySampler(device.device(), image_sampler, nullptr);
        }
    }

    /**
    * In case the viewport size changes during runtime (IE: resizing the game
    * window), this method can be used to set its new size without needing to
    * manually destroy and re-create the object. Instead this method will
    * take care of it.
    */
    void set_size(uint32_t w, uint32_t h) {
        cleanup();
        extent.width = w; extent.height = h;
        init();
    }

    VkSampler &get_sampler() {
        if (!image_sampler_inited) {
            init_sampler();
        }
        return image_sampler;
    }
    VkImage &get_image() {
        return image;
    }
    VkImageView &get_image_view() {
        return image_view;
    }
    uint32_t get_index() const {
        return att_index;
    }
    VkAttachmentReference &get_reference() {
        return att_ref;
    }
    VkAttachmentDescription &get_description() {
        return att_desc;
    }

    // RAII
    Attachment(const Attachment &) = delete;
    Attachment& operator=(const Attachment &) = delete;
    Attachment(Attachment &&) = delete;
    Attachment& operator=(Attachment &&) = delete;
};
