#pragma once

#include "defines.hpp"
#include <nlohmann/json.hpp>
#include <cstdlib>
#include <fstream>
#include <filesystem>

/**
* Configuration manager, loads and saves a JSON configuration file for the
* project. The configuration file is saved in the standard XDG_CONFIG_HOME
* directory, inside a subdirectory bearing the name of the application ID.
* It uses the singleton design pattern to be able to retrieve the same object
* across the project.
*/
class ConfMan {
private:
    nlohmann::json conf;
    bool conf_changed = false;
    std::string CONF_FILE_PATH = "";

    inline static const nlohmann::json BASE_SCHEMA = {
        {
            "graphics",
            {
                {
                    "resolution", {
                        {"w", 1280},
                        {"h", 720}
                    }
                },
                {"fullscreen", false},
                {"window_borders", true},
                {"vsync", true},
                {"view_distance", 20.0},
                {"fov", 50.0},
                {"bloom", true},
                {"fxaa", true}
            },
        },
        {
            "controls", {
                {"look_sensitivity", 0.2}
            }
        },
        {
            "overlay", {
                {"enabled", false},
                {"fps", true},
                {"frame_time", true}
            }
        }
    };

    void init_conf_file_path() {
        // don't redefine
        if (!CONF_FILE_PATH.empty()) return;
        // find config home
        char* c_config_home = std::getenv("XDG_CONFIG_HOME");
        std::string config_home{""};
        if (!c_config_home) {
            char* c_home = std::getenv("HOME");
            if (!c_home) {
                throw std::runtime_error("HOME environment variable is undefined!");
            }
            std::string home{c_home};
            config_home = home + "/.config";
        }
        else {
            config_home = std::string(c_config_home);
        }

        std::string conf_dir = config_home + "/" + APP_ID;
        CONF_FILE_PATH = conf_dir + "/config.json";

        if (!std::filesystem::is_directory(conf_dir)) {
            std::filesystem::create_directory(conf_dir);
        }
    }

    std::string read_file(std::string fpath) {
        assert(!fpath.empty() && "Trying to read a file with no path");
        assert(
            std::filesystem::is_regular_file(fpath) &&
            "Trying to read a file that does not exist"
        );
        std::ifstream ifs(fpath);
        std::string res((std::istreambuf_iterator<char>(ifs)),
            (std::istreambuf_iterator<char>()));
        ifs.close();
        return res;
    }
    
    void write_file(std::string fpath, std::string content) {
        assert(!fpath.empty() && "Trying to write a file with no path");
        std::ofstream ofs(fpath);
        ofs << content;
        ofs.close();
    }

    bool check_conf_compatibility(const nlohmann::json &base, nlohmann::json &target) {
        bool needs_write = false;
        for (auto i: base.items()) {
            // does conf contain this key?
            // if not, add the default from the base schema
            if (!target.contains(i.key())) {
                target[i.key()] = i.value();
                needs_write = true;
            }
            // if it does, check if this key points to another dict
            else {
                // if it does, recurse over it
                if (i.value().is_object()) {
                    bool res = check_conf_compatibility(i.value(), target[i.key()]);
                    needs_write = needs_write || res;
                }
                // otherwise, check types
                // DISABLED: buggy
                /* else { */
                /*     if (i.value().type() != target[i.key()].type()) { */
                /*         target[i.key()] = i.value(); */
                /*         needs_write = true; */
                /*     } */
                /* } */
            }
        }
        return needs_write;
    }

    bool check_conf_compatibility() {
        return check_conf_compatibility(BASE_SCHEMA, conf);
    }

    void load_json() {
        if (std::filesystem::is_regular_file(CONF_FILE_PATH)) {
            conf = nlohmann::json::parse(read_file(CONF_FILE_PATH));
            if (check_conf_compatibility()) {  // true if incompatible
                save_conf();
            }
        }
        else {
            conf = BASE_SCHEMA;
            save_conf();
        }
    }

    ConfMan() {
        init_conf_file_path();
        load_json();
    }

public:
    static ConfMan &get_instance() {
        static ConfMan instance;
        return instance;
    }

    nlohmann::json get_conf() {
        return conf;
    }

    void set_conf(nlohmann::json n_conf) {
        conf = n_conf;
        conf_changed = true;
    }

    void save_conf() {
        write_file(CONF_FILE_PATH, conf.dump(4));
    }
    
    ConfMan(const ConfMan &) = delete;
    ConfMan& operator=(const ConfMan &) = delete;
    ~ConfMan() {
        if (conf_changed) {
            save_conf();
        }
    }
};
