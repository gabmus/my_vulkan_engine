#pragma once


#include "engine_descriptor_pool.hpp"

/**
* Abstraction on VkDescriptorSetLayout. Once a descriptor set layout is
* defined, this class will allocate descriptor sets and create bindings
*/
class EngineDescriptorLayout {
private:
    VkDescriptorSetLayout _layout;
    EngineDevice &device;
    EngineDescriptorPool &pool;
    std::vector<VkDescriptorSetLayoutBinding> bindings;
public:
    EngineDescriptorLayout(
            EngineDevice &device,
            EngineDescriptorPool &pool,
            std::vector<VkDescriptorSetLayoutBinding> bindings
    ) : device{device}, pool{pool}, bindings{bindings} {
        VkDescriptorSetLayoutCreateInfo info{};
        info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        info.pNext = nullptr;
        info.bindingCount = bindings.size();
        info.flags = 0;
        info.pBindings = bindings.data();
        vkCreateDescriptorSetLayout(device.device(), &info, nullptr, &_layout);
    }
    ~EngineDescriptorLayout() {
        vkDestroyDescriptorSetLayout(device.device(), _layout, nullptr);
    }

    void allocate_descriptor_set(VkDescriptorSet &set) {
        VkDescriptorSetAllocateInfo alloc_info{};
        alloc_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        alloc_info.pNext = nullptr;
        alloc_info.descriptorPool = pool.pool();
        alloc_info.descriptorSetCount = 1;
        alloc_info.pSetLayouts = &_layout;
        auto res = vkAllocateDescriptorSets(device.device(), &alloc_info, &set);
        if (res != VK_SUCCESS) {
            std::cerr << "vkAllocateDescriptorSets error code " << res << std::endl;
            throw std::runtime_error("failed to allocate descriptor set!");
        }
    }

    static VkDescriptorSetLayoutBinding create_binding(
            VkDescriptorType descriptor_type,
            VkShaderStageFlags stage_flags,
            uint32_t binding_index
    ) {
        VkDescriptorSetLayoutBinding binding{};
        binding.binding = binding_index;
        binding.descriptorCount = 1;
        binding.descriptorType = descriptor_type;
        binding.stageFlags = stage_flags;
        return binding;
    }

    VkDescriptorSetLayout layout() { return _layout; }

    // RAII
    EngineDescriptorLayout(const EngineDescriptorLayout &) = delete;
    EngineDescriptorLayout& operator=(const EngineDescriptorLayout &) = delete;
    EngineDescriptorLayout(EngineDescriptorLayout &&) = delete;
    EngineDescriptorLayout& operator=(EngineDescriptorLayout &&) = delete;
};
