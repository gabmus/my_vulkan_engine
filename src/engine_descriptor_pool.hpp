#pragma once

#include <array>
#include <vulkan/vulkan_core.h>
#include "engine_device.hpp"

/**
* Minimal wrapper around VkDescriptorPool. Descriptor counts for the various
* descriptor types needed for the project are hardcoded to sane values and
* hidden from the rest of the code.
*/
class EngineDescriptorPool {
private:
    VkDescriptorPool _pool;
    EngineDevice &device;

public:
    EngineDescriptorPool(EngineDevice &device) : device{device} {
        std::array<VkDescriptorPoolSize, 3> pool_sizes;
        pool_sizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        pool_sizes[0].descriptorCount = 50;
        pool_sizes[1].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
        pool_sizes[1].descriptorCount = 50;
        pool_sizes[2].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        pool_sizes[2].descriptorCount = 50;

        VkDescriptorPoolCreateInfo pool_info{};
        pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
        pool_info.flags = 0;
        pool_info.maxSets = 100;
        pool_info.poolSizeCount = static_cast<uint32_t>(pool_sizes.size());
        pool_info.pPoolSizes = pool_sizes.data();
        
        vkCreateDescriptorPool(device.device(), &pool_info, nullptr, &_pool);
    }
    ~EngineDescriptorPool() {
        vkDestroyDescriptorPool(device.device(), _pool, nullptr);
    }

    // RAII
    EngineDescriptorPool(const EngineDescriptorPool &) = delete;
    EngineDescriptorPool& operator=(const EngineDescriptorPool &) = delete;

    VkDescriptorPool pool() { return _pool; }
};
