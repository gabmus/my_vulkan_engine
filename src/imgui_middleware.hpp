#pragma once

#include "libs/imgui/imgui.h"
#include "libs/imgui/imgui_impl_glfw.h"
#include "libs/imgui/imgui_impl_vulkan.h"

#include "engine_swapchain.hpp"
#include "engine_renderer.hpp"
#include "engine_object_quat.hpp"
#include "engine_descriptors.hpp"
#include "vector_dict.hpp"
#include "confman.hpp"
#include "engine_window.hpp"
#include "keyboard_movement_controller.hpp"
#include "fx_manager.hpp"

/**
* Middleware class for managing ImGui and its integration in the engine
*/
class ImguiMiddleware {
private:
    EngineDevice &device;
    EngineRenderer &renderer;
    VkDescriptorPool descriptor_pool;
    EngineRenderer::SwapChainData swapchain_data;
    ConfMan &confman = ConfMan::get_instance();

    void create_descriptor_pool() {
        std::vector<VkDescriptorPoolSize> pool_sizes = {
            { VK_DESCRIPTOR_TYPE_SAMPLER, 1000 },
            { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1000 },
            { VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 1000 },
            { VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1000 },
            { VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, 1000 },
            { VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, 1000 },
            { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1000 },
            { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1000 },
            { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 1000 },
            { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, 1000 },
            { VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 1000 }
        };
        VkDescriptorPoolCreateInfo pool_info = {};
        pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
        pool_info.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
        pool_info.maxSets = 1000; // * IM_ARRAYSIZE(pool_sizes);
        pool_info.poolSizeCount = static_cast<uint32_t>(pool_sizes.size());
        pool_info.pPoolSizes = pool_sizes.data();
        if (vkCreateDescriptorPool(device.device(), &pool_info, nullptr, &descriptor_pool) != VK_SUCCESS) {
            throw std::runtime_error("error creating imgui's descriptor pool");
        };
    }

    void create_fonts_texture() {
        VkCommandBuffer command_buffer = device.begin_single_time_commands();
        ImGui_ImplVulkan_CreateFontsTexture(command_buffer);
        device.end_single_time_commands(command_buffer);
        ImGui_ImplVulkan_DestroyFontUploadObjects();
    }

    bool show_overlay;
    bool show_fps;
    bool show_frame_time;

public:
    ImguiMiddleware(
            GLFWwindow* window,
            EngineDevice &device,
            EngineRenderer &renderer
    ) : device{device}, renderer{renderer} {
        swapchain_data = renderer.get_swap_chain_data();
        create_descriptor_pool();
        IMGUI_CHECKVERSION();
        ImGui::CreateContext();
        ImGuiIO& io = ImGui::GetIO(); (void)io;
        io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard Controls
        ImGui::StyleColorsDark();
        ImGui_ImplGlfw_InitForVulkan(window, true);
        ImGui_ImplVulkan_InitInfo init_info{};
        init_info.DescriptorPool = descriptor_pool;
        init_info.MinImageCount = 2;  // swap chain related, 2 should always work
        init_info.ImageCount = EngineSwapChain::MAX_FRAMES_IN_FLIGHT;
        device.populate_imgui_init_info(init_info);
        ImGui_ImplVulkan_Init(&init_info, swapchain_data.render_pass);
        create_fonts_texture();

        show_overlay = confman.get_conf()["overlay"]["enabled"];
        show_fps = confman.get_conf()["overlay"]["fps"];
        show_frame_time = confman.get_conf()["overlay"]["frame_time"];
    }
    ~ImguiMiddleware() {
        vkDestroyDescriptorPool(device.device(), descriptor_pool, nullptr);
        ImGui_ImplVulkan_Shutdown();
        ImGui_ImplGlfw_Shutdown();
    }

    const std::array<std::string, 3> SPACIAL_DIRECTIONS{"X", "Y", "Z"};

    void draw_transform_manipulation_func(EngineObjectQuat &obj) {
        bool res = false;
        ImGui::Text("%s", obj.name.c_str());
        ImGui::Text("    Translation");
        for (size_t i=0; i<3; i++) {
            res = ImGui::SliderFloat(
                (SPACIAL_DIRECTIONS[i]+"##"+obj.name+"translation").c_str(),
                &obj.transform.translation[i], -5.f, 5.f
            ) || res;
        }
        if (res) {
            obj.set_rigid_body_transform();
        }
    }

    /**
    * Start the drawing process for the ImGui interface.
    *
    * By itself this method draws the FPS and frame time overlays.
    *
    * If the `Esc` key is pressed, it will show some controls, including:
    *
    * - position manipulation sliders for some EngineObjectQuat objects in the
    * scene
    * - light source position, intensity and color
    * - ambient light intensity and color
    * - window size controls
    * - toggles for FXAA and bloom
    * - FOV and view distance
    * - mouse sensitivity
    */
    void begin_draw(
            VDict<EngineObjectQuat> &objects,
            SceneData &scene_data,
            float* fov,
            float* view_distance,
            bool* camera_projection_changed,
            EngineWindow &ewindow,
            KeyboardMovementController &controller,
            FxManager &fxman,
            float frame_time,
            bool show
    ) {
        ImGui_ImplVulkan_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
        if (confman.get_conf()["overlay"]["enabled"]) {
            ImGui::Begin(
                "Overlay", nullptr,
                ImGuiWindowFlags_NoDecoration 
                | ImGuiWindowFlags_NoInputs 
                | ImGuiWindowFlags_NoCollapse
                | ImGuiWindowFlags_NoBackground
            );
            ImGui::SetWindowPos({10, 10});
            ImGui::SetWindowSize({-1, -1});
            ImGui::SetWindowFontScale(1.5);
            if (confman.get_conf()["overlay"]["fps"]) {
                ImGui::Text("FPS: %.0f", 1.f / frame_time);
            }
            if (confman.get_conf()["overlay"]["frame_time"]) {
                ImGui::Text("Frame time %.2f ms", frame_time*1000.f);
            }
            ImGui::End();
        }
        // ImGui::Begin(
        //     "Crosshair", nullptr,
        //     ImGuiWindowFlags_NoDecoration 
        //     | ImGuiWindowFlags_NoInputs 
        //     | ImGuiWindowFlags_NoCollapse
        //     //| ImGuiWindowFlags_NoBackground
        // );
        // ImGui::SetWindowSize({50, 50});
        // ImGui::SetWindowPos({
        //     (ewindow.getExtent().width  / 2.f) - 25.f,
        //     (ewindow.getExtent().height / 2.f) - 25.f
        // });
        // ImGui::End();
        if (show) {
            ImGui::Begin("World manipulation");
            draw_transform_manipulation_func(objects["barrel"]);
            draw_transform_manipulation_func(objects["crate"]);
            ImGui::Text("Light");
            ImGui::SliderFloat("X##light", &scene_data.light_pos.x, -5.f, 5.f);
            ImGui::SliderFloat("Y##light", &scene_data.light_pos.y, -5.f, 5.f);
            ImGui::SliderFloat("Z##light", &scene_data.light_pos.z, -5.f, 5.f);
            ImGui::ColorPicker3("Color##light", &scene_data.light_color.x, 0);
            ImGui::SliderFloat("Intensity##light", &scene_data.light_color.w, 0.f, 30.f);
            ImGui::Text("Ambient Light");
            ImGui::ColorPicker3("Color##ambient", &scene_data.ambient_color.x, 0);
            ImGui::SliderFloat("Intensity##ambient", &scene_data.ambient_color.w, 0.f, 1.f);
            ImGui::End();
            ImGui::Begin("Settings");
            ImGui::Text("GPU: %s", device.get_device_name().c_str());
            ImGui::Text("Graphics");
            ewindow.draw_settings();
            fxman.draw_settings();
            if(ImGui::SliderFloat("FOV", fov, 10.f, 150.f)) {
                auto n_conf = confman.get_conf();
                n_conf["graphics"]["fov"] = *fov;
                confman.set_conf(n_conf);
                *camera_projection_changed = true;
            }
            if(ImGui::SliderFloat("View distance", view_distance, 5.f, 150.f)) {
                auto n_conf = confman.get_conf();
                n_conf["graphics"]["view_distance"] = *view_distance;
                confman.set_conf(n_conf);
                *camera_projection_changed = true;
            }
            controller.draw_settings();
            //ImGui::Checkbox("Noclip", noclip);
            ImGui::Text("Misc");
            if (ImGui::Checkbox("Show overlay", &show_overlay)) {
                auto n_conf = confman.get_conf();
                n_conf["overlay"]["enabled"] = show_overlay;
                confman.set_conf(n_conf);
            }
            if (ImGui::Checkbox("Show FPS", &show_fps)) {
                auto n_conf = confman.get_conf();
                n_conf["overlay"]["fps"] = show_fps;
                confman.set_conf(n_conf);
            }
            if (ImGui::Checkbox("Show frame time", &show_frame_time)) {
                auto n_conf = confman.get_conf();
                n_conf["overlay"]["frame_time"] = show_frame_time;
                confman.set_conf(n_conf);
            }
            ImGui::End();
        }
        ImGui::Render();
    }

    void end_draw(VkCommandBuffer command_buffer) {
        ImGui_ImplVulkan_RenderDrawData(ImGui::GetDrawData(), command_buffer);
    }

    // RAII
    ImguiMiddleware(const ImguiMiddleware &) = delete;
    ImguiMiddleware& operator=(const ImguiMiddleware &) = delete;
    ImguiMiddleware(ImguiMiddleware &&) = delete;
    ImguiMiddleware& operator=(ImguiMiddleware &&) = delete;
};
