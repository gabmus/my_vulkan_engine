#pragma once

#include "defines.hpp"
#include "engine_device.hpp"
#include "engine_buffer.hpp"
#include "libs/vk_mem_alloc.h"
#include <cstring>
#include <memory>
#include <unordered_map>
#include <vulkan/vulkan_core.h>
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <vector>
#include <cassert>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/hash.hpp>

#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h>

/**
* Represents a 3D model, with vertices and indices. It's responsible for
* loading obj files into useful representations for the rest of the engine
*/
class EngineModel {
public:

    struct Vertex {
        glm::vec3 position;
        glm::vec3 normal;
        glm::vec3 color;
        glm::vec2 texCoord;  // also known as UV
        // glm::vec3 emission_color;
        // float emission_intensity;

        bool operator==(const Vertex& other) const {
            return position == other.position &&
                   color == other.color &&
                   normal == other.normal &&
                   texCoord == other.texCoord;
        }
        static std::vector<VkVertexInputBindingDescription> getBindingDescriptions() {
            std::vector<VkVertexInputBindingDescription> binding_descriptions(1);
            binding_descriptions[0].binding = 0;
            binding_descriptions[0].stride = sizeof(Vertex);
            binding_descriptions[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
            return binding_descriptions;
        }
        static std::vector<VkVertexInputAttributeDescription> getAttributeDescriptions() {
            std::vector<VkVertexInputAttributeDescription> attribute_descriptions(4);
            attribute_descriptions[0].binding = 0;
            attribute_descriptions[0].location = 0;
            attribute_descriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
            attribute_descriptions[0].offset = offsetof(Vertex, position);

            attribute_descriptions[1].binding = 0;
            attribute_descriptions[1].location = 1;
            attribute_descriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
            attribute_descriptions[1].offset = offsetof(Vertex, normal);

            attribute_descriptions[2].binding = 0;
            attribute_descriptions[2].location = 2;
            attribute_descriptions[2].format = VK_FORMAT_R32G32B32_SFLOAT;
            attribute_descriptions[2].offset = offsetof(Vertex, color);

            attribute_descriptions[3].binding = 0;
            attribute_descriptions[3].location = 3;
            attribute_descriptions[3].format = VK_FORMAT_R32G32_SFLOAT;
            attribute_descriptions[3].offset = offsetof(Vertex, texCoord);

            return attribute_descriptions;
        }
    };

    struct ModelData {
        std::vector<Vertex> vertices{};
        std::vector<std::uint32_t> indices{};
        std::string texture_path = "";
        std::string normalmap_path = "";

        void load_obj(const std::string &filepath, const std::string &basedir);
    };

    EngineModel(EngineDevice &device, const ModelData &model_data)
            : device{device} {
        createVertexBuffers(model_data.vertices);
        createIndexBuffers(model_data.indices);
        create_texture(model_data.texture_path);
        create_normalmap(model_data.normalmap_path);
        calculate_bounding_box(model_data.vertices);
    }
    ~EngineModel() {}

    void bind(VkCommandBuffer command_buffer) {
        VkBuffer buffers[]  = {vertex_buffer->getBuffer()};
        VkDeviceSize offsets[] = {0};

        vkCmdBindVertexBuffers(command_buffer, 0, 1, buffers, offsets);
        if (has_index_buffer) {
            vkCmdBindIndexBuffer(command_buffer, indices_buffer->getBuffer(), 0, VK_INDEX_TYPE_UINT32);
        }
    }

    void draw(VkCommandBuffer command_buffer) {
        if (has_index_buffer) {
            vkCmdDrawIndexed(command_buffer, index_count, 1, 0, 0, 0);
        }
        else {
            vkCmdDraw(command_buffer, vertex_count, 1, 0, 0);
        }
    }

    static std::unique_ptr<EngineModel> new_model_from_obj(
            EngineDevice &device, const std::string &filepath, const std::string &basedir
    ) {
        ModelData model_data{};
        model_data.load_obj(filepath, basedir);

        return std::make_unique<EngineModel>(device, model_data);
    }

    static std::unique_ptr<EngineModel> new_cube(EngineDevice &device) {
        return new_model_from_obj(device, ASSETS_DIR+"/assets/cube.obj", ASSETS_DIR+"assets");
    }

    std::shared_ptr<TextureData> &get_texture_data() {
        return texture_data;
    }

    std::shared_ptr<TextureData> &get_normalmap_data() {
        return normalmap_data;
    }

    VkSampler &get_sampler() {
        return texture_data->sampler;
    }

    VkImageView &get_image_view() {
        return texture_data->image_view;
    }

    VkSampler &get_normal_sampler() {
        return normalmap_data->sampler;
    }

    VkImageView &get_normal_image_view() {
        return normalmap_data->image_view;
    }
    
    bool has_normalmap() const { return has_normalmap_; }

    glm::vec3 get_bounding_box_size() const { return bounding_box_size; }

    // RAII
    EngineModel(const EngineModel &) = delete;
    void operator=(const EngineModel &) = delete;
private:
    EngineDevice& device;
    std::unique_ptr<EngineBuffer> vertex_buffer;
    uint32_t vertex_count;
    /* TODO create textures */
    /* VkBuffer texture_buffer; */
    /* VmaAllocation texture_buffer_allocation; */
    bool has_index_buffer = false;
    std::unique_ptr<EngineBuffer> indices_buffer;
    uint32_t index_count;

    bool has_normalmap_;

    std::shared_ptr<TextureData> texture_data;
    std::shared_ptr<TextureData> normalmap_data;

    glm::vec3 bounding_box_size;

    void create_texture(const std::string texture_path) {
        texture_data = device.create_texture_image(texture_path);
    }

    void create_normalmap(const std::string texture_path) {
        normalmap_data = device.create_texture_image_normal(texture_path);
        has_normalmap_ = !texture_path.empty();
    }

    void createVertexBuffers(const std::vector<Vertex> &vertices) {
        vertex_count = static_cast<uint32_t>(vertices.size());
        assert(vertex_count >= 3 && "Vertex count must be at least 3");
        VkDeviceSize vertex_size = sizeof(vertices[0]);
        VkDeviceSize buffer_size = vertex_size * vertex_count;

        EngineBuffer staging_buffer{
            device, vertex_size, vertex_count, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT
        };
        staging_buffer.map();
        staging_buffer.writeToBuffer((void *)vertices.data());
        // automatic unmap on staging buffer destroy

        vertex_buffer = std::make_unique<EngineBuffer>(
            device, vertex_size, vertex_count,
            VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
        );

        device.copy_buffer(
            staging_buffer.getBuffer(), vertex_buffer->getBuffer(), buffer_size
        );
    }

    void createIndexBuffers(const std::vector<uint32_t> &indices) {
        index_count = static_cast<uint32_t>(indices.size());
        has_index_buffer = index_count > 0;
        if (!has_index_buffer) return;
        VkDeviceSize index_size = sizeof(indices[0]);
        VkDeviceSize buffer_size = index_size * index_count;

        EngineBuffer staging_buffer{
            device, index_size, index_count, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT
        };
        staging_buffer.map();
        staging_buffer.writeToBuffer((void *)indices.data());

        indices_buffer = std::make_unique<EngineBuffer>(
            device, index_size, index_count,
            VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
        );

        device.copy_buffer(
            staging_buffer.getBuffer(), indices_buffer->getBuffer(), buffer_size
        );
    }

    void calculate_bounding_box(const std::vector<Vertex> &vertices) {
        glm::vec3 cmin = vertices[0].position;
        glm::vec3 cmax = vertices[0].position;
        for (auto vertex: vertices) {
            cmin.x = std::fmin(cmin.x, vertex.position.x);
            cmin.y = std::fmin(cmin.y, vertex.position.y);
            cmin.z = std::fmin(cmin.z, vertex.position.z);

            cmax.x = std::fmax(cmax.x, vertex.position.x);
            cmax.y = std::fmax(cmax.y, vertex.position.y);
            cmax.z = std::fmax(cmax.z, vertex.position.z);
        }

        bounding_box_size = cmax - cmin;
    }
};

namespace std {
    template<> struct hash<EngineModel::Vertex> {
        size_t operator()(EngineModel::Vertex const& vertex) const {
            return (((hash<glm::vec3>()(vertex.position) ^
                    (hash<glm::vec3>()(vertex.color) << 1)) >> 1) ^
                    (hash<glm::vec3>()(vertex.normal) << 1) >> 1) ^
                    (hash<glm::vec2>()(vertex.texCoord) << 1);
        }
    };
}

inline void EngineModel::ModelData::load_obj(const std::string &filepath, const std::string &basedir)  {
    tinyobj::ObjReaderConfig reader_config{};
    /* reader_config.mtl_search_path = basedir; */
    tinyobj::ObjReader reader;

    if (!reader.ParseFromFile(filepath, reader_config)) {
        if (!reader.Error().empty()) {
            std::cerr << "import_obj error: " << reader.Error() << std::endl;
        }
        throw std::runtime_error("Error loading obj!");
    }
    if (!reader.Warning().empty()) {
        std::cerr << "import_obj warning: " << reader.Warning() << std::endl;
    }

    auto& attrib = reader.GetAttrib();
    auto& shapes = reader.GetShapes();
    auto& materials = reader.GetMaterials();

    vertices.clear(); indices.clear();
    std::unordered_map<Vertex, uint32_t> unique_vertices{};
    for (const auto &shape: shapes) {
        for (const auto &index: shape.mesh.indices) {
            Vertex vertex{};
            if (index.vertex_index >= 0) {
                vertex.position = {
                    attrib.vertices[3*index.vertex_index + 0],
                    -attrib.vertices[3*index.vertex_index + 1],
                    -attrib.vertices[3*index.vertex_index + 2],
                };
                vertex.color = {
                    attrib.colors[3*index.vertex_index + 0],
                    attrib.colors[3*index.vertex_index + 1],
                    attrib.colors[3*index.vertex_index + 2],
                };
            }
            if (index.normal_index >= 0) {
                vertex.normal = {
                    attrib.normals[3*index.normal_index + 0],
                    -attrib.normals[3*index.normal_index + 1],
                    -attrib.normals[3*index.normal_index + 2],
                };
            }
            if (index.texcoord_index >= 0) {
                vertex.texCoord = {
                    attrib.texcoords[2*index.texcoord_index + 0],
                    1.f-attrib.texcoords[2*index.texcoord_index + 1],
                };
            }

            if (unique_vertices.count(vertex) == 0) {
                unique_vertices[vertex] = static_cast<uint32_t>(vertices.size());
                vertices.push_back(vertex);
            }
            indices.push_back(unique_vertices[vertex]);
        }
    }
    if (!materials[0].diffuse_texname.empty()) texture_path = basedir + "/" + materials[0].diffuse_texname;
    if (!materials[0].normal_texname.empty()) normalmap_path = basedir + "/" + materials[0].normal_texname;
}
