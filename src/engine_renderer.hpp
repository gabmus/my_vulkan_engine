#pragma once

#include <cassert>
#include <memory>
#include <utility>
#include <vector>
#include <array>
#include <stdexcept>

#include "engine_window.hpp"
#include "engine_device.hpp"
#include "engine_swapchain.hpp"

/**
* Manage command buffers, the swap chain and its current frame.
*
* It's responsible for starting and ending frames, as well as managing the swap
* chain, including re-creating it when the viewport size changes
*/
class EngineRenderer {
private:
    uint32_t current_image_index;
    int current_frame_index{0};
    bool is_frame_started{false};

    EngineWindow& window;
    EngineDevice& device;
    std::unique_ptr<EngineSwapChain> swapChain;
    std::vector<VkCommandBuffer> command_buffers;

    void create_command_buffers() {
        command_buffers.resize(EngineSwapChain::MAX_FRAMES_IN_FLIGHT);
        VkCommandBufferAllocateInfo alloc_info{};
        alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        alloc_info.commandPool = device.get_command_pool();
        alloc_info.commandBufferCount = static_cast<uint32_t>(command_buffers.size());

        if (vkAllocateCommandBuffers(
                device.device(), &alloc_info, command_buffers.data()
        ) != VK_SUCCESS) {
            throw std::runtime_error("failed to allocate command buffers");
        }
    }
    void free_command_buffers() {
        vkFreeCommandBuffers(
            device.device(), device.get_command_pool(),
            static_cast<uint32_t>(command_buffers.size()), command_buffers.data()
        );
        command_buffers.clear();
    }

    bool _swap_chain_changed = false;
    void recreate_swapchain() {
        auto extent = window.getExtent();
        while (extent.width == 0 || extent.height == 0) {
            extent = window.getExtent();
            glfwWaitEvents();
        }

        vkDeviceWaitIdle(device.device());
        if (!swapChain) {
            swapChain = std::make_unique<EngineSwapChain>(device, extent);
        }
        else {
            _swap_chain_changed = true;
            std::shared_ptr<EngineSwapChain> old_swapChain = std::move(swapChain);
            swapChain = std::make_unique<EngineSwapChain>(device, extent, old_swapChain);
            if (!old_swapChain->compare_swap_formats(*swapChain.get())) {
                throw std::runtime_error("Swap chain image (or depth) format has changed");
            }
        }
    }

public:
    EngineRenderer(EngineWindow& window, EngineDevice& device)
            : window{window}, device{device} {
        recreate_swapchain();
        create_command_buffers();
    }
    ~EngineRenderer() {
        free_command_buffers();
    }

    // "resource acquisition is initialization"
    EngineRenderer(const EngineRenderer &) = delete;
    EngineRenderer& operator=(const EngineRenderer &) = delete;

    bool swap_chain_changed() { return _swap_chain_changed; }
    void reset_swap_chain_changed() { _swap_chain_changed = false; }

    /**
    * Starts rendering a new frame, returning the command buffer needed to
    * enqueue the various render passes
    */
    VkCommandBuffer begin_frame() {
        assert(!is_frame_started && "Can't call begin_frame while frame is in progress");
        auto result = swapChain->acquireNextImage(&current_image_index);

        if (result == VK_ERROR_OUT_OF_DATE_KHR) {
            recreate_swapchain();
            return nullptr;
        }
        if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
            throw std::runtime_error("failed to acquire next swap chain image");
            // will need to handle this: can occur on window resize
        }
        is_frame_started = true;
        auto command_buffer = get_current_command_buffer();
        VkCommandBufferBeginInfo begin_info{};
        begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        if (vkBeginCommandBuffer(
                command_buffer, &begin_info
        ) != VK_SUCCESS) {
            throw std::runtime_error("failed to begin command buffer");
        }
        return command_buffer;
    }

    /**
    * Completes the rendering of a frame, submitting the command buffer to
    * the swap chain
    */
    void end_frame() {
        assert(is_frame_started && "Can't end frame if frame is not in progress");
        auto command_buffer = get_current_command_buffer();
        if (vkEndCommandBuffer(command_buffer) != VK_SUCCESS) {
            throw std::runtime_error("failed to record command buffer");
        }

        auto result = swapChain->submitCommandBuffers(
            &command_buffer, &current_image_index
        );
        if (
                result == VK_ERROR_OUT_OF_DATE_KHR ||
                result == VK_SUBOPTIMAL_KHR ||
                window.was_window_resized()
        ) {
            window.reset_window_resized_flag();
            recreate_swapchain();
        } else if (result != VK_SUCCESS) {
            throw std::runtime_error("failed to present swap chain image");
        }
        is_frame_started = false;
        current_frame_index =
            (current_frame_index + 1) % EngineSwapChain::MAX_FRAMES_IN_FLIGHT;
    }

    void begin_swap_chain_render_pass(VkCommandBuffer command_buffer) {
        assert(is_frame_started && "Can't begin render pass if frame is not in progress");
        assert(command_buffer == get_current_command_buffer() && "Can't begin render pass on command buffer for a different frame");

        VkRenderPassBeginInfo render_pass_begin_info{};
        render_pass_begin_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        render_pass_begin_info.renderPass = swapChain->getRenderPass();
        render_pass_begin_info.framebuffer = swapChain->getFrameBuffer(current_image_index);

        render_pass_begin_info.renderArea.offset = {0, 0};
        render_pass_begin_info.renderArea.extent = swapChain->getSwapChainExtent();

        std::array<VkClearValue, 2> clear_values{};
        // this is the background color
        clear_values[0].color = {{0.1f, 0.1f, 0.1f, 1.0f}};
        clear_values[1].depthStencil = {1.0f, 0};
        render_pass_begin_info.clearValueCount = static_cast<uint32_t>(clear_values.size());
        render_pass_begin_info.pClearValues = clear_values.data();

        vkCmdBeginRenderPass(
            command_buffer, &render_pass_begin_info,
            VK_SUBPASS_CONTENTS_INLINE
        );

        VkViewport viewport{};
        viewport.x = 0.0f; viewport.y = 0.0f;
        viewport.width = static_cast<float>(swapChain->getSwapChainExtent().width);
        viewport.height = static_cast<float>(swapChain->getSwapChainExtent().height);
        viewport.minDepth = 0.0f; viewport.maxDepth = 1.0f;
        VkRect2D scissor{{0, 0}, swapChain->getSwapChainExtent()};
        vkCmdSetViewport(command_buffer, 0, 1, &viewport);
        vkCmdSetScissor(command_buffer, 0, 1, &scissor);
    }
    void end_swap_chain_render_pass(VkCommandBuffer command_buffer) {
        assert(is_frame_started && "Can't end render pass if frame is not in progress");
        assert(command_buffer == get_current_command_buffer() && "Can't end render pass on command buffer for a different frame");
        vkCmdEndRenderPass(command_buffer);
    }

    bool is_frame_in_progress() const { return is_frame_started; }
    VkCommandBuffer get_current_command_buffer() const {
        assert(is_frame_started && "Cannot get command buffer when frame is not in progress");
        return command_buffers[current_frame_index];
    }

    VkRenderPass get_swap_chain_render_pass() const {
        return swapChain->getRenderPass();
    }

    float get_aspect_ratio() const {
        return swapChain->extentAspectRatio();
    }

    int get_frame_index() const {
        assert(is_frame_started && "Cannot get frame index when frame is not in progress");
        return current_frame_index;
    }

    size_t get_swap_chain_size() const {
        return swapChain->imageCount();
    }

    struct SwapChainData {
        size_t image_count;
        VkFormat format;
        VkExtent2D extent;
        VkRenderPass render_pass;
    };

    /**
    * Returns various metadata related to the swap chain, useful for other
    * classes that may need this information, without necessarily giving access
    * to the swap chain itself
    */
    SwapChainData get_swap_chain_data() const { return {
        get_swap_chain_size(),
        swapChain->getSwapChainImageFormat(),
        swapChain->getSwapChainExtent(),
        swapChain->getRenderPass()
    }; }
};
