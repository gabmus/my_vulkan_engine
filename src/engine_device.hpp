#pragma once

#include "defines.hpp"
#include "engine_window.hpp"
#include "libs/imgui/imgui_impl_vulkan.h"
#include <memory>
#include <numeric>
#define VMA_IMPLEMENTATION
#include "libs/vk_mem_alloc.h"

#include <iostream>
#include <cstring>
#include <string>
#include <vector>
#include <set>
#include <unordered_set>
#include <vulkan/vulkan_core.h>

#define STB_IMAGE_IMPLEMENTATION
#include "libs/stb_image.h"

struct SwapChainSupportDetails {
    VkSurfaceCapabilitiesKHR capabilities;
    std::vector<VkSurfaceFormatKHR> formats;
    std::vector<VkPresentModeKHR> presentModes;
};

struct QueueFamilyIndices {
    uint32_t graphicsFamily;
    uint32_t presentFamily;
    bool graphicsFamilyHasValue = false;
    bool presentFamilyHasValue = false;
    bool isComplete() { return graphicsFamilyHasValue && presentFamilyHasValue; }
};

// local callback functions
static VKAPI_ATTR VkBool32 VKAPI_CALL debug_callback(
        VkDebugUtilsMessageSeverityFlagBitsEXT message_severity,
        VkDebugUtilsMessageTypeFlagsEXT message_type,
        const VkDebugUtilsMessengerCallbackDataEXT *p_callback_data,
        void *p_user_data
) {
    std::cerr << "validation layer: " << p_callback_data->pMessage << std::endl;
    return VK_FALSE;
}

inline VkResult create_debug_utils_messenger_EXT(
        VkInstance instance,
        const VkDebugUtilsMessengerCreateInfoEXT *p_create_info,
        const VkAllocationCallbacks *p_allocator,
        VkDebugUtilsMessengerEXT *p_debug_messenger
) {
    auto func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(
        instance,
        "vkCreateDebugUtilsMessengerEXT");
    if (func != nullptr) {
        return func(instance, p_create_info, p_allocator, p_debug_messenger);
    } else {
        return VK_ERROR_EXTENSION_NOT_PRESENT;
    }
}

inline void destroy_debug_utils_messenger_EXT(
        VkInstance instance,
        VkDebugUtilsMessengerEXT debug_messenger,
        const VkAllocationCallbacks *p_allocator
) {
    auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(
        instance,
        "vkDestroyDebugUtilsMessengerEXT");
    if (func != nullptr) {
      func(instance, debug_messenger, p_allocator);
    }
}

#define ENGINE_API_VERSION VK_API_VERSION_1_2
#define MISSING_TEXTURE_PATH (ASSETS_DIR+"/assets/missing_texture.png")

struct TextureData {
    bool missing = false;
    std::string cache_key = "";
    std::string path = "";
    VkImage image;
    VmaAllocation allocation;
    VkImageView image_view;
    VkSampler sampler;

    VkDevice &device;
    VmaAllocator &allocator;

    TextureData(VkDevice &device, VmaAllocator &allocator) : device{device}, allocator{allocator} {}
    ~TextureData() {
        cleanup();
    }
    void cleanup() {
        vkDestroySampler(device, sampler, nullptr);
        vkDestroyImageView(device, image_view, nullptr);
        vmaDestroyImage(allocator, image, allocation);
    }

    TextureData(const TextureData&) = delete;
    TextureData& operator=(const TextureData&) = delete;
};

/**
* Abstraction around VkDevice, managing everything to do with the physical
* and logical device, as well as in-device memory allocation and management
*/
class EngineDevice {
private:
    void init_vma() {
        VmaAllocatorCreateInfo allocator_info = {};
        allocator_info.vulkanApiVersion = ENGINE_API_VERSION;
        allocator_info.physicalDevice = physical_device;
        allocator_info.device = device_;
        allocator_info.instance = instance;

        vmaCreateAllocator(&allocator_info, &allocator);
    }

    void create_instance() {
        if (enable_validation_layers && !check_validation_layer_support()) {
            throw std::runtime_error("validation layers requested, but not available!");
        }
        VkApplicationInfo app_info = {};
        app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        app_info.pApplicationName = "GabMus Vulkan Engine App";
        app_info.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
        app_info.pEngineName = "No Engine";
        app_info.engineVersion = VK_MAKE_VERSION(1, 0, 0);
        app_info.apiVersion = ENGINE_API_VERSION;

        VkInstanceCreateInfo create_info = {};
        create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        create_info.pApplicationInfo = &app_info;

        auto extensions = get_required_extensions();
        create_info.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
        create_info.ppEnabledExtensionNames = extensions.data();

        VkDebugUtilsMessengerCreateInfoEXT debugCreateInfo;
        if (enable_validation_layers) {
            create_info.enabledLayerCount = static_cast<uint32_t>(validation_layers.size());
            create_info.ppEnabledLayerNames = validation_layers.data();

            populate_debug_messenger_create_info(debugCreateInfo);
            create_info.pNext = (VkDebugUtilsMessengerCreateInfoEXT *)&debugCreateInfo;
        } else {
            create_info.enabledLayerCount = 0;
            create_info.pNext = nullptr;
        }

        if (vkCreateInstance(&create_info, nullptr, &instance) != VK_SUCCESS) {
            throw std::runtime_error("failed to create instance!");
        }

        has_glfw_required_instance_extensions();
    }
    void setup_debug_messenger() {
        if (!enable_validation_layers) return;
        VkDebugUtilsMessengerCreateInfoEXT createInfo;
        populate_debug_messenger_create_info(createInfo);
        if (create_debug_utils_messenger_EXT(instance, &createInfo, nullptr, &debug_messenger) != VK_SUCCESS) {
            throw std::runtime_error("failed to set up debug messenger!");
        }
    }
    void create_surface() {
        window.create_window_surface(instance, &surface_);
    }
    void pick_physical_device() {
        uint32_t deviceCount = 0;
        vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);
        if (deviceCount == 0) {
            throw std::runtime_error("failed to find GPUs with Vulkan support!");
        }
        std::cout << "Device count: " << deviceCount << std::endl;
        std::vector<VkPhysicalDevice> devices(deviceCount);
        vkEnumeratePhysicalDevices(instance, &deviceCount, devices.data());

        for (const auto &device : devices) {
            if (is_device_suitable(device)) {
                physical_device = device;
                break;
            }
        }

        if (physical_device == VK_NULL_HANDLE) {
            throw std::runtime_error("failed to find a suitable GPU!");
        }

        vkGetPhysicalDeviceProperties(physical_device, &properties);
        minOffsetAlignment = std::lcm(
            properties.limits.minUniformBufferOffsetAlignment,
            properties.limits.nonCoherentAtomSize
        );
        std::cout << "physical device: " << properties.deviceName << std::endl;
    }
    void create_logical_device() {
        QueueFamilyIndices indices = find_queue_families(physical_device);

        std::vector<VkDeviceQueueCreateInfo> queue_create_infos;
        std::set<uint32_t> unique_queue_families = {indices.graphicsFamily, indices.presentFamily};

        float queue_priority = 1.0f;
        for (uint32_t queue_family : unique_queue_families) {
            VkDeviceQueueCreateInfo queue_create_info = {};
            queue_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            queue_create_info.queueFamilyIndex = queue_family;
            queue_create_info.queueCount = 1;
            queue_create_info.pQueuePriorities = &queue_priority;
            queue_create_infos.push_back(queue_create_info);
        }

        VkPhysicalDeviceFeatures device_features = {};
        device_features.samplerAnisotropy = VK_TRUE;
        device_features.geometryShader = VK_TRUE;

        VkDeviceCreateInfo create_info = {};
        create_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;

        create_info.queueCreateInfoCount = static_cast<uint32_t>(queue_create_infos.size());
        create_info.pQueueCreateInfos = queue_create_infos.data();

        create_info.pEnabledFeatures = &device_features;
        create_info.enabledExtensionCount = static_cast<uint32_t>(device_extensions.size());
        create_info.ppEnabledExtensionNames = device_extensions.data();

        // might not really be necessary anymore because device specific validation layers
        // have been deprecated
        if (enable_validation_layers) {
            create_info.enabledLayerCount = static_cast<uint32_t>(validation_layers.size());
            create_info.ppEnabledLayerNames = validation_layers.data();
        } else {
            create_info.enabledLayerCount = 0;
        }

        if (vkCreateDevice(physical_device, &create_info, nullptr, &device_) != VK_SUCCESS) {
            throw std::runtime_error("failed to create logical device!");
        }

        vkGetDeviceQueue(device_, indices.graphicsFamily, 0, &graphics_queue_);
        vkGetDeviceQueue(device_, indices.presentFamily, 0, &present_queue_);
    }
    void create_command_pool() {
        QueueFamilyIndices queue_family_indices = find_physical_queue_families();

        VkCommandPoolCreateInfo pool_info = {};
        pool_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        pool_info.queueFamilyIndex = queue_family_indices.graphicsFamily;
        pool_info.flags =
            VK_COMMAND_POOL_CREATE_TRANSIENT_BIT | VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

        if (vkCreateCommandPool(device_, &pool_info, nullptr, &command_pool) != VK_SUCCESS) {
            throw std::runtime_error("failed to create command pool!");
        }
    }

    // helper functions
    bool is_device_suitable(VkPhysicalDevice device) {
        QueueFamilyIndices indices = find_queue_families(device);

        bool extensions_supported = check_device_extension_support(device);

        bool swap_chain_adequate = false;
        if (extensions_supported) {
            SwapChainSupportDetails swap_chain_support = query_swap_chain_support(device);
            swap_chain_adequate = !swap_chain_support.formats.empty() && !swap_chain_support.presentModes.empty();
        }

        VkPhysicalDeviceFeatures supported_features;
        vkGetPhysicalDeviceFeatures(device, &supported_features);

        return indices.isComplete() && extensions_supported && swap_chain_adequate &&
               supported_features.samplerAnisotropy;
    }
    std::vector<const char *> get_required_extensions() {
        uint32_t glfw_extension_count = 0;
        const char **glfw_extensions;
        glfw_extensions = glfwGetRequiredInstanceExtensions(&glfw_extension_count);

        std::vector<const char *> extensions(glfw_extensions, glfw_extensions + glfw_extension_count);

        if (enable_validation_layers) {
          extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
        }

        return extensions;
    }
    bool check_validation_layer_support() {
        uint32_t layer_count;
        vkEnumerateInstanceLayerProperties(&layer_count, nullptr);

        std::vector<VkLayerProperties> available_layers(layer_count);
        vkEnumerateInstanceLayerProperties(&layer_count, available_layers.data());

        for (const char *layer_name : validation_layers) {
            bool layer_found = false;

            for (const auto &layer_properties : available_layers) {
                if (strcmp(layer_name, layer_properties.layerName) == 0) {
                    layer_found = true;
                    break;
                }
            }

            if (!layer_found) {
                return false;
            }
        }

        return true;
    }
    QueueFamilyIndices find_queue_families(VkPhysicalDevice device) {
        QueueFamilyIndices indices;

        uint32_t queue_family_count = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(device, &queue_family_count, nullptr);

        std::vector<VkQueueFamilyProperties> queue_families(queue_family_count);
        vkGetPhysicalDeviceQueueFamilyProperties(device, &queue_family_count, queue_families.data());

        int i = 0;
        for (const auto &queue_family : queue_families) {
            if (queue_family.queueCount > 0 && queue_family.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
                indices.graphicsFamily = i;
                indices.graphicsFamilyHasValue = true;
            }
            VkBool32 presentSupport = false;
            vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface_, &presentSupport);
            if (queue_family.queueCount > 0 && presentSupport) {
                indices.presentFamily = i;
                indices.presentFamilyHasValue = true;
            }
            if (indices.isComplete()) {
                break;
            }

            i++;
        }

        return indices;
    }
    void populate_debug_messenger_create_info(VkDebugUtilsMessengerCreateInfoEXT &create_info) {
        create_info = {};
        create_info.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
        create_info.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
                                      VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
        create_info.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
                                  VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
                                  VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
        create_info.pfnUserCallback = debug_callback;
        create_info.pUserData = nullptr;  // Optional
    }
    void has_glfw_required_instance_extensions() {
        uint32_t extension_count = 0;
        vkEnumerateInstanceExtensionProperties(nullptr, &extension_count, nullptr);
        std::vector<VkExtensionProperties> extensions(extension_count);
        vkEnumerateInstanceExtensionProperties(nullptr, &extension_count, extensions.data());

        // std::cout << "available extensions:" << std::endl;
        std::unordered_set<std::string> available;
        for (const auto &extension : extensions) {
            // std::cout << "\t" << extension.extensionName << std::endl;
            available.insert(extension.extensionName);
        }

        // std::cout << "required extensions:" << std::endl;
        auto required_extensions = get_required_extensions();
        for (const auto &required : required_extensions) {
            // std::cout << "\t" << required << std::endl;
            if (available.find(required) == available.end()) {
                throw std::runtime_error("Missing required glfw extension");
            }
        }
    }
    bool check_device_extension_support(VkPhysicalDevice device) {
        uint32_t extension_count;
        vkEnumerateDeviceExtensionProperties(device, nullptr, &extension_count, nullptr);

        std::vector<VkExtensionProperties> available_extensions(extension_count);
        vkEnumerateDeviceExtensionProperties(
            device,
            nullptr,
            &extension_count,
            available_extensions.data());

        std::set<std::string> required_extensions(device_extensions.begin(), device_extensions.end());

        for (const auto &extension : available_extensions) {
          required_extensions.erase(extension.extensionName);
        }

        return required_extensions.empty();
    }
    SwapChainSupportDetails query_swap_chain_support(VkPhysicalDevice device) {
        SwapChainSupportDetails details;
        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface_, &details.capabilities);

        uint32_t format_count;
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface_, &format_count, nullptr);

        if (format_count != 0) {
            details.formats.resize(format_count);
            vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface_, &format_count, details.formats.data());
        }

        uint32_t present_mode_count;
        vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface_, &present_mode_count, nullptr);

        if (present_mode_count != 0) {
            details.presentModes.resize(present_mode_count);
            vkGetPhysicalDeviceSurfacePresentModesKHR(
                    device,
                    surface_,
                    &present_mode_count,
                    details.presentModes.data());
        }
        return details;
    }

    VmaAllocator allocator;

    VkInstance instance;
    VkDebugUtilsMessengerEXT debug_messenger;
    VkPhysicalDevice physical_device = VK_NULL_HANDLE;
    EngineWindow &window;
    VkCommandPool command_pool;

    VkDevice device_;
    VkSurfaceKHR surface_;
    VkQueue graphics_queue_;
    VkQueue present_queue_;

    const std::vector<const char *> validation_layers = {"VK_LAYER_KHRONOS_validation"};
    const std::vector<const char *> device_extensions = {VK_KHR_SWAPCHAIN_EXTENSION_NAME};

    std::unordered_map<std::string, std::shared_ptr<TextureData>> texture_cache{};

public:

    std::unordered_map<std::string, std::shared_ptr<TextureData>> &get_texture_cache() {
        return texture_cache;
    }

#ifdef DISABLE_VK_VALIDATION
    const bool enable_validation_layers = false;
#else
    const bool enable_validation_layers = true;
#endif
    EngineDevice(EngineWindow &window) : window{window} {
        create_instance();
        setup_debug_messenger();
        create_surface();
        pick_physical_device();
        create_logical_device();
        init_vma();
        create_command_pool();
        create_missing_texture_data();
    }
    ~EngineDevice() {
        texture_cache.clear();
        vmaDestroyAllocator(allocator);
        vkDestroyCommandPool(device_, command_pool, nullptr);
        vkDestroyDevice(device_, nullptr);
        if (enable_validation_layers) {
            destroy_debug_utils_messenger_EXT(
                instance, debug_messenger, nullptr
            );
        }
        vkDestroySurfaceKHR(instance, surface_, nullptr);
        vkDestroyInstance(instance, nullptr);
    }

    // RAII
    EngineDevice(const EngineDevice &) = delete;
    EngineDevice& operator=(const EngineDevice &) = delete;
    EngineDevice(EngineDevice &&) = delete;
    EngineDevice& operator=(EngineDevice &&) = delete;

    VkCommandPool get_command_pool() { return command_pool; }
    VmaAllocator get_allocator() const { return allocator; }
    VkDevice device() { return device_; }
    VkSurfaceKHR surface() { return surface_; }
    VkQueue graphics_queue() { return graphics_queue_; }
    VkQueue present_queue() { return present_queue_; }

    SwapChainSupportDetails get_swap_chain_support() { return query_swap_chain_support(physical_device); }
    uint32_t find_memory_type(uint32_t type_filter, VkMemoryPropertyFlags properties) {
        VkPhysicalDeviceMemoryProperties mem_properties;
        vkGetPhysicalDeviceMemoryProperties(physical_device, &mem_properties);
        for (uint32_t i = 0; i < mem_properties.memoryTypeCount; i++) {
            if ((type_filter & (1 << i)) &&
                    (mem_properties.memoryTypes[i].propertyFlags & properties) == properties) {
                return i;
            }
        }

        throw std::runtime_error("failed to find suitable memory type!");
    }
    QueueFamilyIndices find_physical_queue_families() { return find_queue_families(physical_device); }
    VkFormat find_supported_format(
            const std::vector<VkFormat> &candidates,
            VkImageTiling tiling,
            VkFormatFeatureFlags features
    ) {
        for (VkFormat format : candidates) {
            VkFormatProperties props;
            vkGetPhysicalDeviceFormatProperties(physical_device, format, &props);

            if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features) {
                return format;
            } else if (
                    tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features) {
                return format;
            }
        }
        throw std::runtime_error("failed to find supported format!");
    }

    std::shared_ptr<TextureData> create_texture_image(std::string img_path) {
        return create_texture_image(img_path, VK_FORMAT_R8G8B8A8_SRGB);
    }

    std::shared_ptr<TextureData> create_texture_image_normal(std::string img_path) {
        return create_texture_image(img_path, VK_FORMAT_R8G8B8A8_UNORM);
    }

#define MISSING_TEXDATA_K "~~~MISSING_TEXDATA~~~"

    std::shared_ptr<TextureData> create_texture_image(std::string img_path, VkFormat texture_format) {
        std::string cache_key = img_path + "~~~format" + std::to_string(texture_format);
        if (texture_cache.find(cache_key) != texture_cache.end()) {
            return texture_cache.at(cache_key);
        }
        if (img_path.empty()) {
            if (texture_cache.find(MISSING_TEXDATA_K) != texture_cache.end()) {
                return texture_cache.at(MISSING_TEXDATA_K);
            }
            img_path = MISSING_TEXTURE_PATH;
            cache_key = MISSING_TEXDATA_K;
        }
        std::shared_ptr<TextureData> texture_data = std::make_shared<TextureData>(device_, allocator);
        texture_data->missing = img_path == MISSING_TEXTURE_PATH;
        texture_data->path = img_path;
        texture_data->cache_key = cache_key;
        int width, height, color_channels;
        stbi_uc* pixels = stbi_load(
            img_path.c_str(),
            &width, &height, &color_channels,
            STBI_rgb_alpha
        );

        if (!pixels) {
            throw std::runtime_error("failed to load texture image "+img_path);
        }

        VkDeviceSize image_size = width * height * 4;

        VkBuffer staging_buffer;
        VmaAllocation staging_buffer_allocation;

        create_buffer(
            image_size,
            VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
            staging_buffer, staging_buffer_allocation
        );

        void *data;
        vmaMapMemory(allocator, staging_buffer_allocation, &data);
        std::memcpy(data, pixels, static_cast<size_t>(image_size));
        vmaUnmapMemory(allocator, staging_buffer_allocation);
        stbi_image_free(pixels);

        create_image(
            static_cast<uint32_t>(width), static_cast<uint32_t>(height),
            texture_format, VK_IMAGE_TILING_OPTIMAL,
            VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
            texture_data->image, texture_data->allocation
        );

        transition_image_layout(
            texture_data->image,
            texture_format,
            VK_IMAGE_LAYOUT_UNDEFINED,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
        );

        copy_buffer_to_image(
            staging_buffer, texture_data->image,
            static_cast<uint32_t>(width), static_cast<uint32_t>(height), 1
        );

        transition_image_layout(
            texture_data->image,
            texture_format,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
        );

        vmaDestroyBuffer(allocator, staging_buffer, staging_buffer_allocation);

        create_image_view(
            texture_format, VK_IMAGE_ASPECT_COLOR_BIT,
            texture_data->image, texture_data->image_view
        );

        create_sampler(texture_data->sampler);

        texture_cache[cache_key] = texture_data;

        return texture_data;
    }

    void create_missing_texture_data() {
        create_texture_image("");
    }

    std::shared_ptr<TextureData> create_cubemap_texture(std::array<std::string, 6> img_paths) {
        std::shared_ptr<TextureData> texture_data = std::make_shared<TextureData>(device_, allocator);
        texture_data->missing = false;
        texture_data->path = "~~~CUBEMAP~~~";
        std::array<VkBuffer, 6> staging_buffers{};
        std::array<VmaAllocation, 6> staging_buffers_allocations{};
        stbi_uc* pixels;
        int width, height, color_channels;
        VkDeviceSize image_size;
        void *data;
        // TODO optimization: use only one staging buffer, and copy images
        //      one by one to it then copy from buffer to image and repeat
        //      in practice, two fors become one
        for (size_t i=0; i<6; i++) {
            assert(!img_paths[i].empty() && "Cannot create cubemap without a proper path");
            pixels = stbi_load(
                img_paths[i].c_str(),
                &width, &height, &color_channels,
                STBI_rgb_alpha
            );

            if (!pixels) {
                throw std::runtime_error("failed to load texture image "+img_paths[i]);
            }

            // yes, I know, this value is overwritten in each cycle, but all images should be the same size, so it's not important
            // plus it's probably faster than using an if and initializing it just once
            image_size = width * height * 4;

            create_buffer(
                image_size,
                VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                staging_buffers[i], staging_buffers_allocations[i]
            );

            vmaMapMemory(allocator, staging_buffers_allocations[i], &data);
            std::memcpy(data, pixels, static_cast<size_t>(image_size));
            vmaUnmapMemory(allocator, staging_buffers_allocations[i]);
            stbi_image_free(pixels);
        }

        VkImageCreateInfo cubemap_info{};
        cubemap_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        cubemap_info.imageType = VK_IMAGE_TYPE_2D;
        cubemap_info.format = VK_FORMAT_R8G8B8A8_SRGB;
        cubemap_info.mipLevels = 1;
        // sizing could be problematic, if error come back here
        cubemap_info.extent.width = width;
        cubemap_info.extent.height = height;
        cubemap_info.extent.depth = 1;
        cubemap_info.arrayLayers = 6;
        cubemap_info.tiling = VK_IMAGE_TILING_OPTIMAL;
        cubemap_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        cubemap_info.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
        cubemap_info.samples = VK_SAMPLE_COUNT_1_BIT;
        cubemap_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        cubemap_info.flags = VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;

        create_image_with_info(
            cubemap_info,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
            texture_data->image,
            texture_data->allocation
        );

        transition_image_layout(
            texture_data->image,
            VK_FORMAT_R8G8B8A8_SRGB,
            VK_IMAGE_LAYOUT_UNDEFINED,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            6, 0
        );

        for (size_t i=0; i<6; i++) {
            copy_buffer_to_image(
                staging_buffers[i], texture_data->image,
                static_cast<uint32_t>(width), static_cast<uint32_t>(height), 1, i
            );
        }

        transition_image_layout(
            texture_data->image,
            VK_FORMAT_R8G8B8A8_SRGB,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
            6, 0
        );

        for (size_t i=0; i<6; i++) {
            vmaDestroyBuffer(allocator, staging_buffers[i], staging_buffers_allocations[i]);
        }

        VkImageViewCreateInfo cubemap_view_info{};
        cubemap_view_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        cubemap_view_info.image = texture_data->image;
        cubemap_view_info.viewType = VK_IMAGE_VIEW_TYPE_CUBE;
        cubemap_view_info.format = VK_FORMAT_R8G8B8A8_SRGB;
        cubemap_view_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        cubemap_view_info.subresourceRange.baseMipLevel = 0;
        cubemap_view_info.subresourceRange.levelCount = 1;
        cubemap_view_info.subresourceRange.baseArrayLayer = 0;
        cubemap_view_info.subresourceRange.layerCount = 6;

        create_image_view_with_info(
            cubemap_view_info, texture_data->image_view
        );

        create_sampler(texture_data->sampler);

        return texture_data;
    }

    void transition_image_layout(
            VkImage &image, VkFormat format, VkImageLayout src_layout,
            VkImageLayout dst_layout,
            uint32_t layer_count=1, uint32_t layer=0,
            uint32_t level_count=1, uint32_t level=0
    ) {
        VkCommandBuffer command_buffer = begin_single_time_commands();
        VkPipelineStageFlags sourceStage;
        VkPipelineStageFlags destinationStage;

        VkImageMemoryBarrier barrier;

        barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        barrier.oldLayout = src_layout;
        barrier.newLayout = dst_layout;
        barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

        barrier.image = image;
        barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        barrier.subresourceRange.baseMipLevel = level;
        barrier.subresourceRange.levelCount = level_count;
        barrier.subresourceRange.baseArrayLayer = layer;
        barrier.subresourceRange.layerCount = layer_count;

        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

        if (src_layout == VK_IMAGE_LAYOUT_UNDEFINED && dst_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
            sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
            destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        }
        else if (src_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && dst_layout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
            barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

            sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
            destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
        }
        else {
            throw std::invalid_argument("unsupported layout transition!");
        }

        vkCmdPipelineBarrier(
            command_buffer,
            sourceStage, destinationStage,
            0,
            0, nullptr,
            0, nullptr,
            1, &barrier
        );
        end_single_time_commands(command_buffer);
    }

    void create_buffer(
            VkDeviceSize size,
            VkBufferUsageFlags usage,
            VkMemoryPropertyFlags properties,
            VkBuffer &buffer,
            VmaAllocation &allocation
    ) {
        create_buffer(size, usage, properties, VMA_MEMORY_USAGE_UNKNOWN, buffer, allocation);
    }

    // Buffer Helper Functions
    void create_buffer(
            VkDeviceSize size,
            VkBufferUsageFlags usage,
            VkMemoryPropertyFlags properties,
            VmaMemoryUsage memory_usage,
            VkBuffer &buffer,
            VmaAllocation &allocation
    ) {
        VkBufferCreateInfo buffer_info{};
        buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        buffer_info.size = size;
        buffer_info.usage = usage;
        buffer_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

        VmaAllocationCreateInfo alloc_info{};
        // this flag is to be able to copy memory from gpu to cpu
        // TODO this is sus
        alloc_info.usage = VMA_MEMORY_USAGE_UNKNOWN;
        /* alloc_info.usage = VMA_MEMORY_USAGE_CPU_TO_GPU; */
        alloc_info.requiredFlags = properties;
        vmaCreateBuffer(allocator, &buffer_info, &alloc_info, &buffer, &allocation, VK_NULL_HANDLE);
    }
    VkCommandBuffer begin_single_time_commands() {
        VkCommandBufferAllocateInfo alloc_info{};
        alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        alloc_info.commandPool = command_pool;
        alloc_info.commandBufferCount = 1;

        VkCommandBuffer command_buffer;
        vkAllocateCommandBuffers(device_, &alloc_info, &command_buffer);

        VkCommandBufferBeginInfo begin_info{};
        begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

        vkBeginCommandBuffer(command_buffer, &begin_info);
        return command_buffer;
    }
    void end_single_time_commands(VkCommandBuffer command_buffer) {
        vkEndCommandBuffer(command_buffer);

        VkSubmitInfo submit_info{};
        submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submit_info.commandBufferCount = 1;
        submit_info.pCommandBuffers = &command_buffer;

        vkQueueSubmit(graphics_queue_, 1, &submit_info, VK_NULL_HANDLE);
        vkQueueWaitIdle(graphics_queue_);

        vkFreeCommandBuffers(device_, command_pool, 1, &command_buffer);
    }
    void copy_buffer(VkBuffer src_buffer, VkBuffer dst_buffer, VkDeviceSize size) {
        VkCommandBuffer command_buffer = begin_single_time_commands();

        VkBufferCopy copy_region{};
        copy_region.srcOffset = 0;    // Optional
        copy_region.dstOffset = 0;    // Optional
        copy_region.size = size;
        vkCmdCopyBuffer(command_buffer, src_buffer, dst_buffer, 1, &copy_region);

        end_single_time_commands(command_buffer);
    }
    void copy_buffer_to_image(
        VkBuffer buffer, VkImage image, uint32_t width, uint32_t height,
        uint32_t layer_count=1,
        uint32_t layer=0
    ) {
        VkCommandBuffer commandBuffer = begin_single_time_commands();

        VkBufferImageCopy region{};
        region.bufferOffset = 0;
        region.bufferRowLength = 0;
        region.bufferImageHeight = 0;

        region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        region.imageSubresource.mipLevel = 0;
        region.imageSubresource.baseArrayLayer = layer;
        region.imageSubresource.layerCount = layer_count;

        region.imageOffset = {0, 0, 0};
        region.imageExtent = {width, height, 1};

        vkCmdCopyBufferToImage(
            commandBuffer,
            buffer,
            image,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            1,
            &region
        );
        end_single_time_commands(commandBuffer);
    }

    void create_image(
            uint32_t width, uint32_t height, VkFormat format,
            VkImageTiling tiling, VkImageUsageFlags usage,
            VkMemoryPropertyFlags properties,
            VkImage &image, VmaAllocation &image_memory_allocation
    ) {
        VkImageCreateInfo image_info{};
        image_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        image_info.imageType = VK_IMAGE_TYPE_2D;
        image_info.extent.width = width;
        image_info.extent.height = height;
        image_info.extent.depth = 1;
        image_info.mipLevels = 1;
        image_info.arrayLayers = 1;
        image_info.format = format;
        image_info.tiling = tiling;
        image_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        image_info.usage = usage;
        image_info.samples = VK_SAMPLE_COUNT_1_BIT;
        image_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

        create_image_with_info(
            image_info, properties, image, image_memory_allocation
        );
    }

    void create_sampler(
            VkSampler &sampler
    ) {
        VkSamplerCreateInfo sampler_info{};
        sampler_info.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
        sampler_info.magFilter = VK_FILTER_LINEAR;
        sampler_info.minFilter = VK_FILTER_LINEAR;
        sampler_info.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
        sampler_info.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
        sampler_info.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
        sampler_info.anisotropyEnable = VK_TRUE;
        sampler_info.maxAnisotropy = properties.limits.maxSamplerAnisotropy;
        sampler_info.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
        sampler_info.unnormalizedCoordinates = VK_FALSE;
        sampler_info.compareEnable = VK_FALSE;
        sampler_info.compareOp = VK_COMPARE_OP_ALWAYS;
        sampler_info.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
        sampler_info.mipLodBias = 0.0f;
        sampler_info.minLod = 0.0f;
        sampler_info.maxLod = 0.0f;

        create_sampler_with_info(sampler_info, sampler);
    }

    void create_sampler_with_info(
            const VkSamplerCreateInfo &sampler_info,
            VkSampler &sampler
    ) {
        if (vkCreateSampler(
            device_, &sampler_info, nullptr, &sampler
        ) != VK_SUCCESS) {
            throw std::runtime_error("failed to create sampler!");
        }
    }
    
    void create_image_with_info(
            const VkImageCreateInfo &image_info,
            VkMemoryPropertyFlags properties,
            VkImage &image,
            VmaAllocation &image_memory_allocation
    ) {
        /* VkMemoryRequirements mem_requirements; */
        /* vkGetImageMemoryRequirements(device_, image, &mem_requirements); */
        VmaAllocationCreateInfo alloc_create_info{};
        alloc_create_info.usage = VMA_MEMORY_USAGE_GPU_ONLY;
        alloc_create_info.requiredFlags = properties;
        VmaAllocationInfo alloc_info{};
        /* alloc_info.size = mem_requirements.size; */
        /* alloc_info.memoryType = find_memory_type(mem_requirements.memoryTypeBits, properties); */
        /* alloc_info.deviceMemory = image_memory; */
        if (vmaCreateImage(
            allocator, &image_info, &alloc_create_info,
            &image, &image_memory_allocation, &alloc_info
        ) != VK_SUCCESS) {
            throw std::runtime_error("failed to create image!");
        }
    }

    void create_image_view(
            VkFormat format,
            VkImageAspectFlags aspect_mask,
            VkImage &image,
            VkImageView &image_view
    ) {
        VkImageViewCreateInfo view_info{};
        view_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        view_info.image = image;
        view_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
        view_info.format = format;
        view_info.subresourceRange.aspectMask = aspect_mask;
        view_info.subresourceRange.baseMipLevel = 0;
        view_info.subresourceRange.levelCount = 1;
        view_info.subresourceRange.baseArrayLayer = 0;
        view_info.subresourceRange.layerCount = 1;
        create_image_view_with_info(view_info, image_view);
    }

    void create_image_view_with_info(
            const VkImageViewCreateInfo &view_info,
            VkImageView &image_view
    ) {
        if (vkCreateImageView(
                device_, &view_info, nullptr, &image_view
        ) != VK_SUCCESS) {
            throw std::runtime_error("failed to create texture image view!");
        }
    }

    void destroy_buffer(VkBuffer &buffer, VmaAllocation &allocation) {
        vmaDestroyBuffer(allocator, buffer, allocation);
    }

    void destroy_image(VkImage &image, VmaAllocation &allocation, VkImageView &image_view, VkSampler &sampler) {
        vkDestroySampler(device_, sampler, nullptr);
        vkDestroyImageView(device_, image_view, nullptr);
        vmaDestroyImage(allocator, image, allocation);
    }

    void destroy_texture(TextureData &tdata) {
        vkDestroySampler(device_, tdata.sampler, nullptr);
        vkDestroyImageView(device_, tdata.image_view, nullptr);
        vmaDestroyImage(allocator, tdata.image, tdata.allocation);
    }

    size_t pad_uniform_buffer_size(size_t original_size) {
        // Calculate required alignment based on minimum device offset alignment
        size_t minUboAlignment = properties.limits.minUniformBufferOffsetAlignment;
        size_t alignedSize = original_size;
        if (minUboAlignment > 0) {
            alignedSize = (alignedSize + minUboAlignment - 1) & ~(minUboAlignment - 1);
        }
        return alignedSize;
    }

    VkPhysicalDeviceProperties properties;
    VkDeviceSize minOffsetAlignment;

    void populate_imgui_init_info(ImGui_ImplVulkan_InitInfo &info) {
        assert(info.DescriptorPool && "imgui init info: descriptor pool must be populated");
        assert(info.ImageCount && "imgui init info: image count must be populated");
        info.Instance = instance;
        info.PhysicalDevice = physical_device;
        info.Device = device_;
        info.QueueFamily = 0; // THIS IS SUS
        info.Queue = graphics_queue_;
        info.PipelineCache = VK_NULL_HANDLE;
        info.Allocator = nullptr;
        info.CheckVkResultFn = [](VkResult res) {
            if (res != VK_SUCCESS) {
                std::cerr << "err code: " << res << std::endl;
                throw std::runtime_error("imgui encountered a vulkan error! return code above");
            }
        };
    }

    std::string get_device_name() { return properties.deviceName; }
};
