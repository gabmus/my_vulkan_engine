#pragma once

#include "engine_object.hpp"
#include "engine_window.hpp"
#include "confman.hpp"
#include <GLFW/glfw3.h>
#include <limits>
#include "libs/imgui/imgui.h"


/**
* Regular keyboard key or mouse button
*/
class NormalKey {
protected:
    int key{-1};
    bool is_mouse{false};
public:
    NormalKey(int key, bool is_mouse=false) : key{key}, is_mouse{is_mouse} {}
    bool pressed(GLFWwindow* window) {
        const auto get_key_f = is_mouse ? glfwGetMouseButton : glfwGetKey;

        return get_key_f(window, key) == GLFW_PRESS;
    }
    int get_key() const { return key; }
};


/**
* Keyboard key or mouse button that doesn't keep on activating if held
*/
class DebouncedKey : NormalKey {
private:
    bool debouncer{false};
public:
    DebouncedKey(int key, bool is_mouse=false) : NormalKey(key, is_mouse) {}
    bool pressed(GLFWwindow* window) {
        const auto get_key_f = is_mouse ? glfwGetMouseButton : glfwGetKey;

        if (get_key_f(window, key) == GLFW_PRESS && !debouncer) {
            debouncer = true;
            return true;
        } else if (get_key_f(window, key) == GLFW_RELEASE && debouncer) {
            debouncer = false;
        }
        return false;
    }
};


/**
* General purpose input system built around GLFW.
*
* Despite the name, it manages both keyboard and mouse movement, and it
* allows for player/camera movement as well as other functions
*/
class KeyboardMovementController {
public:
    KeyboardMovementController(GLFWwindow* window) : window{window} {
        // if available, use raw mouse input
        if (glfwRawMouseMotionSupported()) {
            glfwSetInputMode(window, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);
        }
        set_mouse_lock(true);
        int win_width, win_height;
        get_win_size(&win_width, &win_height);
        float half_w = win_width/2.f;
        float half_h = win_height/2.f;
        glfwSetCursorPos(window, half_w, half_h);

        // conf
        mouse_sensitivity = confman.get_conf()["controls"]["look_sensitivity"];
    }

    /**
    * Mappings defined inside a structure
    */
    struct KeyMappings {
        NormalKey move_left{GLFW_KEY_A};
        NormalKey move_right{GLFW_KEY_D};
        NormalKey move_forward{GLFW_KEY_W};
        NormalKey move_backward{GLFW_KEY_S};

        NormalKey move_up{GLFW_KEY_E};
        NormalKey move_down{GLFW_KEY_Q};

        NormalKey look_left{GLFW_KEY_LEFT};
        NormalKey look_right{GLFW_KEY_RIGHT};
        NormalKey look_up{GLFW_KEY_UP};
        NormalKey look_down{GLFW_KEY_DOWN};

        DebouncedKey toggle_mouse_lock{GLFW_KEY_ESCAPE};
        DebouncedKey fire{GLFW_MOUSE_BUTTON_LEFT, true};
        DebouncedKey grab{GLFW_KEY_F};
    };


    /**
    * Actions that can occur from user input. An instance of this structure
    * is returned by KeyboardMovementController::move_in_plane_xz()
    */
    struct Actions {
        bool fire{false};
        bool grab{false};
    };

    /**
    * Movement can only happen if mouse lock is enabled.
    *
    * Mouse lock can be toggled by pressing the `Esc` key. This will also show
    * the ImGui menus, as defined in ImguiMiddleware.
    *
    * If a button associated with an action defined in Actions is pressed,
    * then its value is set to true, so that EngineApp can react accordingly
    */
    Actions move_in_plane_xz(float dt, EngineObject &obj) {
        Actions actions{};
        bool moved = false;
        glm::vec3 rotate{0};
        double xpos, ypos;
        int win_width, win_height;
        get_win_size(&win_width, &win_height);
        float half_w = win_width/2.f;
        float half_h = win_height/2.f;
        // mouse look
        if (keys.toggle_mouse_lock.pressed(window)) {
            set_mouse_lock(!mouse_locked());
            glfwSetCursorPos(window, half_w, half_h);
        }
        if (mouse_locked()) {
            if (keys.look_left.pressed(window)) {
                rotate.y -= 1.f;
            }
            if (keys.look_right.pressed(window)) {
                rotate.y += 1.f;
            }
            if (keys.look_down.pressed(window)) {
                rotate.x -= 1.f;
            }
            if (keys.look_up.pressed(window)) {
                rotate.x += 1.f;
            }
            glfwGetCursorPos(window, &xpos, &ypos);
            glfwSetCursorPos(window, half_w, half_h);

            float rel_mouse_x = xpos - half_w;
            float rel_mouse_y = ypos - half_h;
            // rotation and mouse movement are inverted
            rotate.y += rel_mouse_x * mouse_sensitivity;
            rotate.x -= rel_mouse_y * mouse_sensitivity;

            actions.fire = keys.fire.pressed(window);
            actions.grab = keys.grab.pressed(window);
        }

        // this if checks whether rotate is zero, this is a compact way to do it
        // if the rotate vector is zero, everything blows up
        if (glm::dot(rotate, rotate) > std::numeric_limits<float>::epsilon()) {
            obj.transform.rotation += dt * rotate;
            moved = true;
        }
        
        // avoids the object going upside down, similarly to how the camera behaves in fps
        obj.transform.rotation.x = glm::clamp(obj.transform.rotation.x, -1.5f, 1.5f);
        obj.transform.rotation.y = glm::mod(obj.transform.rotation.y, glm::two_pi<float>());

        float yaw = obj.transform.rotation.y;
        const glm::vec3 forward_direction{sin(yaw), 0.f, cos(yaw)};
        const glm::vec3 right_direction{forward_direction.z, 0.f, -forward_direction.x};
        const glm::vec3 up_direction{0.f, -1.f, 0.f};

        glm::vec3 move_direction{0.f};
        if (mouse_locked()) {
            if (keys.move_left.pressed(window)) {
                move_direction -= right_direction;
            }
            if (keys.move_right.pressed(window)) {
                move_direction += right_direction;
            }
            if (keys.move_backward.pressed(window)) {
                move_direction -= forward_direction;
            }
            if (keys.move_forward.pressed(window)) {
                move_direction += forward_direction;
            }
            if (keys.move_down.pressed(window)) {
                move_direction -= up_direction;
            }
            if (keys.move_up.pressed(window)) {
                move_direction += up_direction;
            }
        }

        if (glm::dot(move_direction, move_direction) > std::numeric_limits<float>::epsilon()) {
            obj.transform.translation += move_speed * dt * glm::normalize(move_direction);
            moved = true;
        }
        /* if (moved) obj.set_rigid_body_transform(); */

        return actions;
    }

    bool mouse_locked() {
        return glfwGetInputMode(window, GLFW_CURSOR) == GLFW_CURSOR_DISABLED;
    }

    /**
    * Uses ImGui to draw settings relative to the input mappings (currently
    * only mouse sensitivity)
    *
    * This method is used in ImguiMiddleware
    */
    void draw_settings() {
        ImGui::Text("Controls");
        if(ImGui::InputFloat("Mouse sensitivity", &mouse_sensitivity)) {
            auto n_conf = confman.get_conf();
            n_conf["controls"]["look_sensitivity"] = mouse_sensitivity;
            confman.set_conf(n_conf);
        };
        ImGui::Text("TODO: keybindings");
    }
private:
    GLFWwindow* window;

    KeyMappings keys{};
    float move_speed{3.f};
    float mouse_sensitivity{.1f};
    ConfMan &confman = ConfMan::get_instance();

    void get_win_size(int *width, int *height) {
        glfwGetWindowSize(window, width, height);
    }
    void set_mouse_lock(bool state) {
        // locks the cursor, perfect for fps
        if (state) {
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        }    
        else {
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        }
    }
};
