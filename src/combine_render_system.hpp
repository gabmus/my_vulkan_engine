#pragma once

#include "engine_descriptors.hpp"
#include "engine_pipeline.hpp"

#include <memory>


/**
* Render system that combines two Offscreen frames into one
*
* Mostly for use with post-processing effects in FxManager
*/
class CombineRenderSystem {
private:
    EngineDevice &device;
    EngineDescriptorManager &descman;
    std::unique_ptr<EnginePipeline> pipeline;
    VkPipelineLayout pipeline_layout;

    void create_pipeline_layout() {
        VkPipelineLayoutCreateInfo pipeline_layout_info{};
        pipeline_layout_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        std::vector<VkDescriptorSetLayout> descriptor_sets_layouts{};
        descman.get_combine_layouts(descriptor_sets_layouts);
        pipeline_layout_info.setLayoutCount = descriptor_sets_layouts.size();
        pipeline_layout_info.pSetLayouts = descriptor_sets_layouts.data();
        pipeline_layout_info.pushConstantRangeCount = 0;
        pipeline_layout_info.pPushConstantRanges = nullptr;

        if (vkCreatePipelineLayout(
                device.device(), &pipeline_layout_info, nullptr, &pipeline_layout
        ) != VK_SUCCESS) {
            throw std::runtime_error("failed to create pipeline layout");
        }
    }

    void create_pipeline(VkRenderPass render_pass) {
        assert(pipeline_layout != nullptr && "Cannot create pipeline before pipeline layout");

        PipelineConfigInfo pipeline_config{};
        EnginePipeline::default_pipeline_config_info(pipeline_config);
        pipeline_config.render_pass = render_pass;
        pipeline_config.pipeline_layout = pipeline_layout;
        std::vector<PipelineShaderData> shaders_data = {
            {
                SHADERS_DIR+"/fullscreen.vert.spv",
                VK_SHADER_STAGE_VERTEX_BIT
            },
            {
                SHADERS_DIR+"/combine.frag.spv",
                VK_SHADER_STAGE_FRAGMENT_BIT
            }
        };

        VkPipelineVertexInputStateCreateInfo emptyInputState;
        emptyInputState.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        emptyInputState.vertexAttributeDescriptionCount = 0;
        emptyInputState.pVertexAttributeDescriptions = nullptr;
        emptyInputState.vertexBindingDescriptionCount = 0;
        emptyInputState.pVertexBindingDescriptions = nullptr;

        pipeline = std::make_unique<EnginePipeline>(
            device,
            shaders_data,
            pipeline_config,
            emptyInputState
        );
    }

public:

    CombineRenderSystem(
            EngineDevice &device,
            EngineDescriptorManager &descman,
            VkRenderPass render_pass
    ) : device{device}, descman{descman} {
        create_pipeline_layout();
        create_pipeline(render_pass);
    }

    ~CombineRenderSystem() {
        vkDestroyPipelineLayout(device.device(), pipeline_layout, nullptr);
    }

    // RAII
    CombineRenderSystem(const CombineRenderSystem &) = delete;
    CombineRenderSystem& operator=(const CombineRenderSystem &) = delete;

    /**
    * Runs the draw step
    *
    * @param scene_offscreen the Offscreen object representing the unmodified
    *        scene
    * @param fx_offscreen the Offscreen object that has been altered by a
    *        certain effect (IE: blurred in case of a bloom effect)
    */
    void render(
            VkCommandBuffer command_buffer,
            Offscreen &scene_offscreen,
            Offscreen &fx_offscreen
    ) {
        pipeline->bind(command_buffer);
        descman.bind_combine_descriptor_set(
            command_buffer, pipeline_layout, scene_offscreen, fx_offscreen
        );
        vkCmdDraw(command_buffer, 3, 1, 0, 0);
    }
};
