#pragma once

#include <vulkan/vulkan_core.h>
#include <vector>
#include "engine_object_quat.hpp"
#include "engine_camera.hpp"
#include "engine_renderer.hpp"
#include "offscreen.hpp"
#include "engine_descriptor_pool.hpp"
#include "engine_descriptor_layout.hpp"

struct SceneData {
    glm::vec4 ambient_color{1.f, 1.f, 1.f, 0.05f};
    glm::vec4 light_color{1.f, 1.f, 1.f, 10.f};  // w is for intensity
    glm::vec4 light_pos{1.f, -3.f, -2.f, 0.f};
};

/**
* Mega-class responsible for handling everything to do with descriptors.
* The purpose of this class is to isolate code with similar purpose which would
* otherwise be scattered throughout the project.
*/
class EngineDescriptorManager {
private:
    EngineDevice &device;
    std::vector<EngineObjectQuat> &objects;

    size_t swap_chain_size{0};

public:
    std::unique_ptr<EngineBuffer> camera_buffers;
    std::unique_ptr<EngineBuffer> scene_buffers;
    std::shared_ptr<TextureData> skybox_texdata;
    EngineDescriptorPool descriptor_pool;

    std::shared_ptr<EngineDescriptorLayout> global_descriptor_set_layout;
    std::shared_ptr<EngineDescriptorLayout> model_descriptor_set_layout;
    std::shared_ptr<EngineDescriptorLayout> generic_texture_descriptor_set_layout;

    VkDescriptorSet skybox_descriptor_set;
    std::vector<VkDescriptorSet> global_descriptor_sets;

    EngineDescriptorManager(
            EngineDevice &device, size_t swap_chain_size,
            std::vector<EngineObjectQuat> &objects,
            std::shared_ptr<TextureData> skybox_texture_data
    ) : device{device}, objects{objects}, swap_chain_size{swap_chain_size},
        skybox_texdata{skybox_texture_data},
        descriptor_pool{EngineDescriptorPool{device}} {
        init();
    }
    
    ~EngineDescriptorManager() {
        cleanup();
    }

    void cleanup() {
        global_descriptor_set_layout.reset();
        model_descriptor_set_layout.reset();
        generic_texture_descriptor_set_layout.reset();
    }

    static VkWriteDescriptorSet create_write_descriptor_buffer(
            VkDescriptorSet dest_descriptor_set,
            VkDescriptorType descriptor_type,
            VkDescriptorBufferInfo* descriptor_buffer_info,
            uint32_t binding_index
    ) {
        VkWriteDescriptorSet desc_set_write{};
        desc_set_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        desc_set_write.pNext = nullptr;
        desc_set_write.dstBinding = binding_index;
        desc_set_write.dstSet = dest_descriptor_set;
        desc_set_write.descriptorCount = 1;
        desc_set_write.descriptorType = descriptor_type;
        desc_set_write.pBufferInfo = descriptor_buffer_info;

        return desc_set_write;
    }

    VkWriteDescriptorSet create_write_descriptor_image(
            VkDescriptorSet dest_descriptor_set,
            VkDescriptorType descriptor_type,
            VkDescriptorImageInfo* descriptor_image_info,
            uint32_t binding_index
    ) {
        VkWriteDescriptorSet desc_set_write{};
        desc_set_write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        desc_set_write.pNext = nullptr;
        desc_set_write.dstBinding = binding_index;
        desc_set_write.dstSet = dest_descriptor_set;
        desc_set_write.descriptorCount = 1;
        desc_set_write.descriptorType = descriptor_type;
        desc_set_write.pImageInfo = descriptor_image_info;

        return desc_set_write;
    }

#define MISSING_TDS_K "~~~MISSING_TDS~~~"

    std::unordered_map<std::string, VkDescriptorSet> model_descriptor_sets_cache{};

    void init_object_descriptor_set(EngineObjectQuat &obj) {
        if (obj.model->get_texture_data()->missing && obj.model->get_normalmap_data()->missing) {
            obj.texture_desc_set = get_missing_model_desc_set();
        }
        else {
            auto k = get_object_cache_key(obj);
            init_model_descriptor_set(
                k,
                obj.model->get_texture_data()->image_view,
                obj.model->get_texture_data()->sampler,
                obj.model->get_normalmap_data()->image_view,
                obj.model->get_normalmap_data()->sampler
            );
            obj.texture_desc_set = get_cached_model_desc_set(k);
        }
    }

    void init_model_descriptor_set(
            std::string desc_cache_key,
            VkImageView tex_image_view,
            VkSampler tex_sampler,
            VkImageView nrm_image_view,
            VkSampler nrm_sampler
    ) {
        model_descriptor_sets_cache[desc_cache_key] = VK_NULL_HANDLE;
        model_descriptor_set_layout->allocate_descriptor_set(
            model_descriptor_sets_cache[desc_cache_key]
        );

        VkDescriptorImageInfo texture_desc_image_info{};
        texture_desc_image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        texture_desc_image_info.imageView = tex_image_view;
        texture_desc_image_info.sampler = tex_sampler;

        VkDescriptorImageInfo normal_desc_image_info{};
        normal_desc_image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        normal_desc_image_info.imageView = nrm_image_view;
        normal_desc_image_info.sampler = nrm_sampler;

        std::vector<VkWriteDescriptorSet> descriptor_writes = {
            // sampler write
            create_write_descriptor_image(
                model_descriptor_sets_cache[desc_cache_key], VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                &texture_desc_image_info, 0
            ),
            create_write_descriptor_image(
                model_descriptor_sets_cache[desc_cache_key], VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                &normal_desc_image_info, 1
            )
        };

        vkUpdateDescriptorSets(
            device.device(),
            descriptor_writes.size(), descriptor_writes.data(),
            0, nullptr
        );
    }

    std::string get_object_cache_key(EngineObjectQuat &obj) {
        return obj.model->get_texture_data()->cache_key +
               obj.model->get_normalmap_data()->cache_key;
    }

    VkDescriptorSet *get_missing_model_desc_set() {
        return &model_descriptor_sets_cache[MISSING_TDS_K];
    }

    VkDescriptorSet *get_cached_model_desc_set(std::string key) {
        return &model_descriptor_sets_cache[key];
    }

    void init_model_descriptor_sets() {
        std::shared_ptr<TextureData> texture_data;
        std::shared_ptr<TextureData> normalmap_data;
        std::string desc_cache_key;

        init_model_descriptor_set(
            MISSING_TDS_K,
            device.get_texture_cache().at(MISSING_TEXDATA_K)->image_view,
            device.get_texture_cache().at(MISSING_TEXDATA_K)->sampler,
            device.get_texture_cache().at(MISSING_TEXDATA_K)->image_view,
            device.get_texture_cache().at(MISSING_TEXDATA_K)->sampler
        );
        for (size_t i = 0; i < objects.size(); i++) {
            texture_data = objects[i].model->get_texture_data();
            normalmap_data = objects[i].model->get_normalmap_data();
            desc_cache_key = get_object_cache_key(objects[i]);
            if (model_descriptor_sets_cache.find(desc_cache_key) != model_descriptor_sets_cache.end()) {
                objects[i].texture_desc_set = &model_descriptor_sets_cache[desc_cache_key];
                continue;
            }
            else if (texture_data->missing && normalmap_data->missing) {
                objects[i].texture_desc_set = get_missing_model_desc_set();
                continue;
            }
            init_model_descriptor_set(
                desc_cache_key,
                texture_data->image_view,
                texture_data->sampler,
                normalmap_data->image_view,
                normalmap_data->sampler
            );

            objects[i].texture_desc_set = &model_descriptor_sets_cache[desc_cache_key];
        }
    }

    void init_skybox_descriptor_set() {
        generic_texture_descriptor_set_layout->allocate_descriptor_set(
            skybox_descriptor_set
        );

        VkDescriptorImageInfo skybox_tex_desc_image_info{};
        skybox_tex_desc_image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        skybox_tex_desc_image_info.imageView = skybox_texdata->image_view;
        skybox_tex_desc_image_info.sampler = skybox_texdata->sampler;

        std::vector<VkWriteDescriptorSet> descriptor_writes = {
            create_write_descriptor_image(
                skybox_descriptor_set, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                &skybox_tex_desc_image_info, 0
            )
        };

        vkUpdateDescriptorSets(
            device.device(),
            descriptor_writes.size(), descriptor_writes.data(),
            0, nullptr
        );
    }

    void init_fullscreen_descriptor_set(Offscreen &offscreen) {
        if (!offscreen.descriptor_set_initialized) {
            generic_texture_descriptor_set_layout->allocate_descriptor_set(
                offscreen.descriptor_set
            );
            offscreen.descriptor_set_initialized = true;
        }

        VkDescriptorImageInfo fullscreen_tex_desc_image_info{};
        fullscreen_tex_desc_image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        fullscreen_tex_desc_image_info.imageView = offscreen.get_image_view();
        fullscreen_tex_desc_image_info.sampler = offscreen.get_sampler();

        std::vector<VkWriteDescriptorSet> descriptor_writes = {
            create_write_descriptor_image(
                offscreen.descriptor_set, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                &fullscreen_tex_desc_image_info, 0
            )
        };

        vkUpdateDescriptorSets(
            device.device(),
            descriptor_writes.size(), descriptor_writes.data(),
            0, nullptr
        );
    }

    void set_object_descriptor_set(EngineObjectQuat &obj) {
        std::string desc_cache_key = obj.model->get_texture_data()->cache_key +
                                     obj.model->get_normalmap_data()->cache_key;
        if (model_descriptor_sets_cache.find(desc_cache_key) != model_descriptor_sets_cache.end()) {
            obj.texture_desc_set = &model_descriptor_sets_cache[desc_cache_key];
        }
        else {
            obj.texture_desc_set = &model_descriptor_sets_cache[MISSING_TDS_K];
        }
    }

    void init_global_descriptor_sets() {
        size_t count = swap_chain_size;
        global_descriptor_sets.resize(count);
        // create buffers
        //// camera buffers
        camera_buffers = std::make_unique<EngineBuffer>(
            device,
            sizeof(EngineCamera::UniformCameraData),
            count,
            VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
            device.minOffsetAlignment
        );
        //// scene buffers
        scene_buffers = std::make_unique<EngineBuffer>(
            device,
            sizeof(SceneData),
            count,
            VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
            device.minOffsetAlignment
        );
        camera_buffers->map();
        scene_buffers->map();
        for (size_t i = 0; i < count; i++) {
            // allocate one descriptor per frame
            global_descriptor_set_layout->allocate_descriptor_set(
                global_descriptor_sets[i]
            );

            //camera
            VkDescriptorBufferInfo camera_desc_buffer_info = camera_buffers->descriptorInfoForIndex(i);
            // scene
            VkDescriptorBufferInfo scene_desc_buffer_info = scene_buffers->descriptorInfoForIndex(i);

            std::vector<VkWriteDescriptorSet> descriptor_writes = {
                // camera write
                create_write_descriptor_buffer(
                    global_descriptor_sets[i], VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, // _DYNAMIC,
                    &camera_desc_buffer_info, 0
                ),
                // scene write
                create_write_descriptor_buffer(
                    global_descriptor_sets[i], VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, // _DYNAMIC,
                    &scene_desc_buffer_info, 1
                )
            };

            vkUpdateDescriptorSets(
                device.device(),
                descriptor_writes.size(), descriptor_writes.data(),
                0, nullptr
            );
        }
    }

    void init() {
        global_descriptor_set_layout = std::make_shared<EngineDescriptorLayout>(
            device, descriptor_pool,
            std::vector<VkDescriptorSetLayoutBinding>{
                EngineDescriptorLayout::create_binding(
                    VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, // _DYNAMIC,
                    VK_SHADER_STAGE_VERTEX_BIT,
                    0
                ),
                EngineDescriptorLayout::create_binding(
                    VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, // _DYNAMIC,
                    VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
                    1
                )
            }
        );
        
        model_descriptor_set_layout = std::make_shared<EngineDescriptorLayout>(
            device, descriptor_pool,
            std::vector<VkDescriptorSetLayoutBinding>{
                EngineDescriptorLayout::create_binding(
                    VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                    VK_SHADER_STAGE_FRAGMENT_BIT,
                    0
                ),
                EngineDescriptorLayout::create_binding(
                    VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                    VK_SHADER_STAGE_FRAGMENT_BIT,
                    1
                )
            }
        );

        generic_texture_descriptor_set_layout = std::make_shared<EngineDescriptorLayout>(
            device, descriptor_pool,
            std::vector<VkDescriptorSetLayoutBinding>{
                EngineDescriptorLayout::create_binding(
                    VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                    VK_SHADER_STAGE_FRAGMENT_BIT,
                    0
                )
            }
        );

        // global descriptor set
        init_global_descriptor_sets();

        // model descriptor set
        init_model_descriptor_sets();

        // skybox descriptor set
        init_skybox_descriptor_set();

        // fullscreen descriptor set
        // handled separately: size depends on framebuffer
    }

    void get_regular_layouts(std::vector<VkDescriptorSetLayout> &layouts) {
        layouts.resize(2);
        layouts[0] = global_descriptor_set_layout->layout();
        layouts[1] = model_descriptor_set_layout->layout();
    }

    void get_skybox_layouts(std::vector<VkDescriptorSetLayout> &layouts) {
        layouts.resize(2);
        layouts[0] = global_descriptor_set_layout->layout();
        layouts[1] = generic_texture_descriptor_set_layout->layout();
    }

    void get_lights_layouts(std::vector<VkDescriptorSetLayout> &layouts) {
        layouts.resize(1);
        layouts[0] = global_descriptor_set_layout->layout();
    }

    void get_fullscreen_layouts(std::vector<VkDescriptorSetLayout> &layouts) {
        layouts.resize(1);
        layouts[0] = generic_texture_descriptor_set_layout->layout();
    }

    void get_combine_layouts(std::vector<VkDescriptorSetLayout> &layouts) {
        layouts.resize(2);
        layouts[0] = generic_texture_descriptor_set_layout->layout();
        layouts[1] = generic_texture_descriptor_set_layout->layout();
    }

    void bind_descriptor_set(
            VkCommandBuffer command_buffer,
            VkPipelineLayout pipeline_layout,
            uint32_t frame_index
    ) {
        // auto offsets = get_global_descriptor_set_offsets(frame_index);
        vkCmdBindDescriptorSets(
            command_buffer,
            VK_PIPELINE_BIND_POINT_GRAPHICS,
            pipeline_layout, 0, 1,
            &global_descriptor_sets[frame_index],
            0, nullptr
            // offsets.size(),   // dynamic offset count
            // offsets.data()
        );
    }

    void bind_skybox_descriptor_set(
        VkCommandBuffer command_buffer,
        VkPipelineLayout pipeline_layout,
        uint32_t frame_index
    ) {
        // auto offsets = get_global_descriptor_set_offsets(frame_index);
        vkCmdBindDescriptorSets(
            command_buffer,
            VK_PIPELINE_BIND_POINT_GRAPHICS,
            pipeline_layout, 0, 1,
            &global_descriptor_sets[frame_index],
            0, nullptr
            // offsets.size(),  // dynamic offset count
            // offsets.data()
        );
        vkCmdBindDescriptorSets(
            command_buffer,
            VK_PIPELINE_BIND_POINT_GRAPHICS,
            pipeline_layout, 1, 1,
            &skybox_descriptor_set,
            0, nullptr
        );
    }

    void bind_fullscreen_descriptor_set(
        VkCommandBuffer command_buffer,
        VkPipelineLayout pipeline_layout,
        Offscreen &offscreen
    ) {
        vkCmdBindDescriptorSets(
            command_buffer,
            VK_PIPELINE_BIND_POINT_GRAPHICS,
            pipeline_layout, 0, 1,
            &offscreen.descriptor_set,
            0, nullptr
        );
    }

    void bind_combine_descriptor_set(
        VkCommandBuffer command_buffer,
        VkPipelineLayout pipeline_layout,
        Offscreen &scene_offscreen,
        Offscreen &fx_offscreen
    ) {
        vkCmdBindDescriptorSets(
            command_buffer,
            VK_PIPELINE_BIND_POINT_GRAPHICS,
            pipeline_layout, 0, 1,
            &scene_offscreen.descriptor_set,
            0, nullptr
        );
        vkCmdBindDescriptorSets(
            command_buffer,
            VK_PIPELINE_BIND_POINT_GRAPHICS,
            pipeline_layout, 1, 1,
            &fx_offscreen.descriptor_set,
            0, nullptr
        );
    }

    void bind_texture(
            VkCommandBuffer command_buffer,
            VkPipelineLayout pipeline_layout,
            EngineObjectQuat& object
    ) {
        vkCmdBindDescriptorSets(
            command_buffer,
            VK_PIPELINE_BIND_POINT_GRAPHICS,
            pipeline_layout, 1, 1,
            object.texture_desc_set,
            0, nullptr
        );
    }

    void copy_camera_data(EngineCamera::UniformCameraData &cam_data, uint32_t frame_index) {
        camera_buffers->writeToIndex(&cam_data, frame_index);
        camera_buffers->flushIndex(frame_index);
    }

    void copy_scene_data(SceneData &scene_data, uint32_t frame_index) {
        scene_buffers->writeToIndex(&scene_data, frame_index);
        scene_buffers->flushIndex(frame_index);
    }

    // RAII
    EngineDescriptorManager(const EngineDescriptorManager &) = delete;
    EngineDescriptorManager& operator=(const EngineDescriptorManager &) = delete;
    EngineDescriptorManager(EngineDescriptorManager &&) = delete;
    EngineDescriptorManager& operator=(EngineDescriptorManager &&) = delete;
};
