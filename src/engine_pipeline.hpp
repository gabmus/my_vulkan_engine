#pragma once

#include <cassert>
#include <fstream>
#include <string>
#include <vector>
#include <stdexcept>
#include <vulkan/vulkan_core.h>

#include "engine_device.hpp"
#include "engine_model.hpp"

struct PipelineConfigInfo {
    PipelineConfigInfo(const PipelineConfigInfo&) = delete;
    PipelineConfigInfo& operator=(const PipelineConfigInfo&) = delete;

    VkPipelineViewportStateCreateInfo viewport_info;
    VkPipelineInputAssemblyStateCreateInfo input_assembly_info;
    VkPipelineRasterizationStateCreateInfo rasterization_info;
    VkPipelineMultisampleStateCreateInfo multisample_info;
    VkPipelineColorBlendAttachmentState color_blend_attachment;
    VkPipelineColorBlendStateCreateInfo color_blend_info;
    VkPipelineDepthStencilStateCreateInfo depth_stencil_info;
    std::vector<VkDynamicState> dynamic_state_enables;
    VkPipelineDynamicStateCreateInfo dynamic_state_info;
    VkPipelineLayout pipeline_layout = nullptr;
    VkRenderPass render_pass = nullptr;
    uint32_t subpass = 0;
};

struct PipelineShaderData {
    std::string file_path;
    VkShaderStageFlagBits stage;
    VkShaderModule shader_module;
};

/**
* Represents a rendering pipeline. It provides a default for the extensive
* amount of options that can be used when creating a pipeline. This default
* can then be tweaked according to the needs of the specific pipeline.
*/
class EnginePipeline {
private:
    void create_shader_stage_info(
            VkPipelineShaderStageCreateInfo &info,
            VkShaderStageFlagBits stage,
            VkShaderModule &module
    ) {
        info.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        info.stage = stage;
        info.module = module;
        info.pName = "main";
        info.flags = 0;
        info.pNext = nullptr;
        info.pSpecializationInfo = nullptr;
    }

    static std::vector<char> read_file(const std::string& file_path) {
        // ate: at the end
        std::ifstream fd{file_path, std::ios::ate | std::ios::binary};
        if (!fd.is_open()) {
            throw std::runtime_error(
                "EnginePipeline: failed to open file " + file_path
            );
        }

        // tellg: get last position, but since we're at the end of the file, we get the size
        size_t file_size = static_cast<size_t>(fd.tellg());

        std::vector<char> out_buf(file_size);
        fd.seekg(0);
        fd.read(out_buf.data(), file_size);
        fd.close();
        return out_buf;
    }

    void create_graphics_pipeline(const PipelineConfigInfo &config_info) {
        auto binding_descriptions = EngineModel::Vertex::getBindingDescriptions();
        auto attribute_descriptions = EngineModel::Vertex::getAttributeDescriptions();
        VkPipelineVertexInputStateCreateInfo vertex_input_info{};
        vertex_input_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        vertex_input_info.vertexAttributeDescriptionCount =
            static_cast<uint32_t>(attribute_descriptions.size());
        vertex_input_info.vertexBindingDescriptionCount =
            static_cast<uint32_t>(binding_descriptions.size());
        vertex_input_info.pVertexAttributeDescriptions = attribute_descriptions.data();
        vertex_input_info.pVertexBindingDescriptions = binding_descriptions.data();
        create_graphics_pipeline(
            config_info,
            vertex_input_info
        );
    }

    void create_graphics_pipeline(
            const PipelineConfigInfo &config_info,
            VkPipelineVertexInputStateCreateInfo vertex_input_info
    ) {
        assert(
            config_info.pipeline_layout !=VK_NULL_HANDLE &&
            "Cannot create graphics pipeline: no pipeline_layout provided in config_info"
        );
        assert(
            config_info.render_pass !=VK_NULL_HANDLE &&
            "Cannot create graphics pipeline: no render_pass provided in config_info"
        );
        std::vector<VkPipelineShaderStageCreateInfo> shader_stages;
        shader_stages.resize(shaders_data.size());
        for (int i=0; i<shaders_data.size(); i++) {
            auto code = read_file(shaders_data[i].file_path);
            create_shader_module(code, &shaders_data[i].shader_module);
            create_shader_stage_info(shader_stages[i], shaders_data[i].stage, shaders_data[i].shader_module);
        }

        VkGraphicsPipelineCreateInfo pipeline_info{};
        pipeline_info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        pipeline_info.stageCount = shader_stages.size();
        pipeline_info.pStages = shader_stages.data();
        pipeline_info.pVertexInputState = &vertex_input_info;
        pipeline_info.pInputAssemblyState = &config_info.input_assembly_info;
        pipeline_info.pViewportState = &config_info.viewport_info;
        pipeline_info.pRasterizationState = &config_info.rasterization_info;
        pipeline_info.pMultisampleState = &config_info.multisample_info;
        pipeline_info.pColorBlendState = &config_info.color_blend_info;
        pipeline_info.pDepthStencilState = &config_info.depth_stencil_info;
        pipeline_info.pDynamicState = &config_info.dynamic_state_info;

        pipeline_info.layout = config_info.pipeline_layout;
        pipeline_info.renderPass = config_info.render_pass;
        pipeline_info.subpass = config_info.subpass;

        pipeline_info.basePipelineHandle = VK_NULL_HANDLE;
        pipeline_info.basePipelineIndex = -1;

        if (vkCreateGraphicsPipelines(
                device.device(), VK_NULL_HANDLE, 1, &pipeline_info, nullptr, &graphics_pipeline
        ) != VK_SUCCESS) {
            throw std::runtime_error("failed to create graphics pipeline");
        }
    }

    void create_shader_module(
            const std::vector<char>& code,
            VkShaderModule* shader_module
    ) {
        VkShaderModuleCreateInfo create_info{};
        create_info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
        create_info.codeSize = code.size();
        create_info.pCode = reinterpret_cast<const uint32_t*>(code.data());

        if (vkCreateShaderModule(
                device.device(), &create_info, nullptr, shader_module
        ) != VK_SUCCESS) {
            throw std::runtime_error("failed to create shader module");
        }
    }

    EngineDevice &device;
    VkPipeline graphics_pipeline;
    std::vector<PipelineShaderData> shaders_data;
    std::vector<VkShaderModule> shader_modules;

public:
    /**
    * Initialize the graphics pipeline, given the appropriate
    * PipelineShaderData objects representing the various stages of the
    * pipeline and the appropriate shader code to be run, as well as the
    * appropriate configuration for the specific pipeline
    *
    * Example usage from engine_render_system.hpp:
    *
    * \snippet src/engine_render_system.hpp Creating a regular Pipeline
    */
    EnginePipeline(
            EngineDevice &device,
            std::vector<PipelineShaderData> shaders_data,
            const PipelineConfigInfo &config_info,
            VkPipelineVertexInputStateCreateInfo vertex_input_info
    ): device{device}, shaders_data{shaders_data} {
        create_graphics_pipeline(config_info, vertex_input_info);
    }

    EnginePipeline(
            EngineDevice &device,
            std::vector<PipelineShaderData> shaders_data,
            const PipelineConfigInfo &config_info
    ): device{device}, shaders_data{shaders_data} {
        create_graphics_pipeline(config_info);
    }
    ~EnginePipeline() {
        for (auto sd: shaders_data) {
            vkDestroyShaderModule(device.device(), sd.shader_module, nullptr);
        }
        vkDestroyPipeline(device.device(), graphics_pipeline, nullptr);
    }

    void bind(VkCommandBuffer command_buffer) {
        vkCmdBindPipeline(
            command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
            graphics_pipeline
        );
    }

    // RAII
    EnginePipeline(const EnginePipeline &) = delete;
    EnginePipeline& operator= (const EnginePipeline &) = delete;

    static void default_pipeline_config_info(PipelineConfigInfo& config_info) {
        config_info.input_assembly_info.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        // every 3 vertices we pass in the input assembly pipeline stage are grouped as triangles
        config_info.input_assembly_info.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        config_info.input_assembly_info.primitiveRestartEnable = VK_FALSE;

        config_info.viewport_info.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        config_info.viewport_info.viewportCount = 1;
        config_info.viewport_info.pViewports = nullptr;
        config_info.viewport_info.scissorCount = 1;
        config_info.viewport_info.pScissors = nullptr;

        config_info.rasterization_info.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
        config_info.rasterization_info.depthClampEnable = VK_FALSE;
        config_info.rasterization_info.rasterizerDiscardEnable = VK_FALSE;
        config_info.rasterization_info.polygonMode = VK_POLYGON_MODE_FILL;
        config_info.rasterization_info.lineWidth = 1.0f;
        // enabling this results in big performance benefits, we'll get back to this
        config_info.rasterization_info.cullMode = VK_CULL_MODE_NONE;
        config_info.rasterization_info.frontFace = VK_FRONT_FACE_CLOCKWISE;
        config_info.rasterization_info.depthBiasEnable = VK_FALSE;
        config_info.rasterization_info.depthBiasConstantFactor = 0.0f;
        config_info.rasterization_info.depthBiasClamp = 0.0f;
        config_info.rasterization_info.depthBiasSlopeFactor = 0.0f;

        config_info.multisample_info.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        config_info.multisample_info.sampleShadingEnable = VK_FALSE;
        config_info.multisample_info.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
        config_info.multisample_info.minSampleShading = 1.0f;
        config_info.multisample_info.pSampleMask = nullptr;
        config_info.multisample_info.alphaToCoverageEnable = VK_FALSE;
        config_info.multisample_info.alphaToOneEnable = VK_FALSE;

        config_info.color_blend_attachment.colorWriteMask =
            VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT |
            VK_COLOR_COMPONENT_A_BIT;
        config_info.color_blend_attachment.blendEnable = VK_FALSE;
        config_info.color_blend_attachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;  // Optional
        config_info.color_blend_attachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;  // Optional
        config_info.color_blend_attachment.colorBlendOp = VK_BLEND_OP_ADD;  // Optional
        config_info.color_blend_attachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;  // Optional
        config_info.color_blend_attachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;  // Optional
        config_info.color_blend_attachment.alphaBlendOp = VK_BLEND_OP_ADD;  // Optional
     
        config_info.color_blend_info.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        config_info.color_blend_info.logicOpEnable = VK_FALSE;
        config_info.color_blend_info.logicOp = VK_LOGIC_OP_COPY;  // Optional
        config_info.color_blend_info.attachmentCount = 1;
        config_info.color_blend_info.pAttachments = &config_info.color_blend_attachment;
        config_info.color_blend_info.blendConstants[0] = 0.0f;  // Optional
        config_info.color_blend_info.blendConstants[1] = 0.0f;  // Optional
        config_info.color_blend_info.blendConstants[2] = 0.0f;  // Optional
        config_info.color_blend_info.blendConstants[3] = 0.0f;  // Optional
     
        config_info.depth_stencil_info.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
        config_info.depth_stencil_info.depthTestEnable = VK_TRUE;
        config_info.depth_stencil_info.depthWriteEnable = VK_TRUE;
        config_info.depth_stencil_info.depthCompareOp = VK_COMPARE_OP_LESS;
        config_info.depth_stencil_info.depthBoundsTestEnable = VK_FALSE;
        config_info.depth_stencil_info.minDepthBounds = 0.0f;  // Optional
        config_info.depth_stencil_info.maxDepthBounds = 1.0f;  // Optional
        config_info.depth_stencil_info.stencilTestEnable = VK_FALSE;
        config_info.depth_stencil_info.front = {};  // Optional
        config_info.depth_stencil_info.back = {};  // Optional

        config_info.dynamic_state_enables = {
            VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR
        };
        config_info.dynamic_state_info.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
        config_info.dynamic_state_info.pDynamicStates = config_info.dynamic_state_enables.data();
        config_info.dynamic_state_info.dynamicStateCount =
            static_cast<uint32_t>(config_info.dynamic_state_enables.size());
        config_info.dynamic_state_info.flags = 0;
    }
};
