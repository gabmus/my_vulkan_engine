#pragma once
#include <stdexcept>
#include <string>
#define GLFW_INCLUDE_VULKAN
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <GLFW/glfw3.h>

#include "confman.hpp"
#include "libs/imgui/imgui.h"


/**
* Wrapper around the GLFW window system. This class manages the window itself,
* while input is managed by KeyboardMovementController
*/
class EngineWindow {
private:
    GLFWwindow* window;
    std::string app_name;
    int width;
    int height;
    bool framebuffer_resized = false;
    ConfMan &confman = ConfMan::get_instance();

    static void framebuffer_resize_callback(GLFWwindow *window, int w, int h) {
        auto engine_window = reinterpret_cast<EngineWindow*>(glfwGetWindowUserPointer(window));
        engine_window->framebuffer_resized = true;
        engine_window->width = w;
        engine_window->height = h;
        engine_window->update_confman_resolution(w, h);
    }

    void update_confman_resolution(int w, int h) {
        auto n_conf = confman.get_conf();
        n_conf["graphics"]["resolution"]["w"] = w;
        n_conf["graphics"]["resolution"]["h"] = h;
        confman.set_conf(n_conf);
    }

    void init_window() {
        glfwInit();
        // no api = dont create an OPENGL context, we need vulkan :)
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
        glfwWindowHint(
            GLFW_DECORATED,
            confman.get_conf()["graphics"]["window_borders"] ? GLFW_TRUE : GLFW_FALSE
        );
        // third param is for making window fullscreen, need to specify a monitor
        // TODO: implement fullscreen
        // confman.get_conf()["graphics"]["fullscreen"]
        window = glfwCreateWindow(
            width, height, app_name.c_str(), nullptr, nullptr
        );
        glfwSetWindowUserPointer(window, this);
        glfwSetFramebufferSizeCallback(window, framebuffer_resize_callback);

        /* glm::mat4 matrix; */
        /* glm::vec4 vec; */
        /* auto test = matrix*vec; */
    }

    void on_window_close() {
        glfwDestroyWindow(window);
        glfwTerminate();
    }

    void resize(int w, int h) {
        glfwSetWindowSize(window, w, h);
    }

public:
    // member initializer: don't bother doing boring assignment in normal code
    EngineWindow(std::string app_name) : app_name{app_name} {
        width = confman.get_conf()["graphics"]["resolution"]["w"];
        height = confman.get_conf()["graphics"]["resolution"]["h"];
        init_window();
    }

    ~EngineWindow() {
        on_window_close();
    }

    // RAII
    EngineWindow(const EngineWindow &) = delete;
    EngineWindow &operator = (const EngineWindow &) = delete;

    bool was_window_resized() { return framebuffer_resized; }
    void reset_window_resized_flag() { framebuffer_resized = false; }

    bool should_close() { return glfwWindowShouldClose(window); }
    void create_window_surface(VkInstance instance, VkSurfaceKHR* surface) {
        if (glfwCreateWindowSurface(instance, window, nullptr, surface) != VK_SUCCESS) {
            throw std::runtime_error("failed to create window surface");
        }
    }

    VkExtent2D getExtent() {
        return {static_cast<uint32_t>(width), static_cast<uint32_t>(height)};
    }

    /**
    * Uses ImGui to draw settings relative to the window resolution
    *
    * This method is used in ImguiMiddleware
    */
    void draw_settings() {
        ImGui::Text("Resolution");
        int w = width;
        int h = height;
        if(
                ImGui::InputInt(
                    "Width", &w, 1, 100,
                    ImGuiInputTextFlags_EnterReturnsTrue
                ) ||
                ImGui::InputInt(
                    "Height", &h, 1, 100,
                    ImGuiInputTextFlags_EnterReturnsTrue
                )
        ) {
            w = std::max(50, abs(w)); h = std::max(50, abs(h));
            auto n_conf = confman.get_conf();
            n_conf["graphics"]["resolution"]["w"] = w;
            n_conf["graphics"]["resolution"]["h"] = h;
            confman.set_conf(n_conf);
            resize(w, h);
        }
    }

    GLFWwindow* get_glfw_window() const { return window; }
};
