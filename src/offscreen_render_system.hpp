#pragma once

#include "engine_descriptors.hpp"
#include "engine_pipeline.hpp"

#include <memory>


/**
* Render System used to run render passes that require data from off screen
* render targets, represented by Offscreen objects
*
* Largely used in FxManager
*/
class OffscreenRenderSystem {
private:
    EngineDevice &device;
    EngineDescriptorManager &descman;
    std::unique_ptr<EnginePipeline> pipeline;
    VkPipelineLayout pipeline_layout;
    std::string frag_path;

    void create_pipeline_layout() {
        VkPipelineLayoutCreateInfo pipeline_layout_info{};
        pipeline_layout_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        std::vector<VkDescriptorSetLayout> descriptor_sets_layouts{};
        descman.get_fullscreen_layouts(descriptor_sets_layouts);
        pipeline_layout_info.setLayoutCount = descriptor_sets_layouts.size();
        pipeline_layout_info.pSetLayouts = descriptor_sets_layouts.data();
        pipeline_layout_info.pushConstantRangeCount = 0;
        pipeline_layout_info.pPushConstantRanges = nullptr;

        if (vkCreatePipelineLayout(
                device.device(), &pipeline_layout_info, nullptr, &pipeline_layout
        ) != VK_SUCCESS) {
            throw std::runtime_error("failed to create pipeline layout");
        }
    }

    void create_pipeline(VkRenderPass render_pass, uint32_t subpass=0) {
        assert(pipeline_layout != nullptr && "Cannot create pipeline before pipeline layout");

        PipelineConfigInfo pipeline_config{};
        EnginePipeline::default_pipeline_config_info(pipeline_config);
        pipeline_config.render_pass = render_pass;
        pipeline_config.pipeline_layout = pipeline_layout;
        pipeline_config.subpass = subpass;
        std::vector<PipelineShaderData> shaders_data = {
            {
                SHADERS_DIR+"/fullscreen.vert.spv",
                VK_SHADER_STAGE_VERTEX_BIT
            },
            {
                frag_path,
                VK_SHADER_STAGE_FRAGMENT_BIT
            }
        };

        VkPipelineVertexInputStateCreateInfo emptyInputState;
        emptyInputState.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        emptyInputState.vertexAttributeDescriptionCount = 0;
        emptyInputState.pVertexAttributeDescriptions = nullptr;
        emptyInputState.vertexBindingDescriptionCount = 0;
        emptyInputState.pVertexBindingDescriptions = nullptr;

        pipeline = std::make_unique<EnginePipeline>(
            device,
            shaders_data,
            pipeline_config,
            emptyInputState
        );
    }

public:

    OffscreenRenderSystem(
            EngineDevice &device,
            EngineDescriptorManager &descman,
            VkRenderPass render_pass,
            std::string frag_path,
            uint32_t subpass=0
    ) : device{device}, descman{descman}, frag_path{frag_path} {
        create_pipeline_layout();
        create_pipeline(render_pass, subpass);
    }

    ~OffscreenRenderSystem() {
        vkDestroyPipelineLayout(device.device(), pipeline_layout, nullptr);
    }

    // RAII
    OffscreenRenderSystem(const OffscreenRenderSystem &) = delete;
    OffscreenRenderSystem& operator=(const OffscreenRenderSystem &) = delete;

    void render(
            VkCommandBuffer command_buffer, Offscreen &source_offscreen
    ) {
        pipeline->bind(command_buffer);
        descman.bind_fullscreen_descriptor_set(command_buffer, pipeline_layout, source_offscreen);
        vkCmdDraw(command_buffer, 3, 1, 0, 0);
    }
};
