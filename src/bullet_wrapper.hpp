#pragma once


#include "btBulletDynamicsCommon.h"
#include "engine_object_quat.hpp"
#include "vector_dict.hpp"
#include <vector>


enum ColliderShape {
    BOX,
    CYLINDER,
    SPHERE,
    CAPSULE
};

/**
* Wrapper around the Bullet physics engine.
*/
class BulletWrapper {
private:
    btDefaultCollisionConfiguration* collision_conf;
    btCollisionDispatcher* dispatcher;
    btBroadphaseInterface* broadphase;
    btSequentialImpulseConstraintSolver* solver;
    btDiscreteDynamicsWorld* dynamics_world;

    void step_obj(EngineObjectQuat &obj) {
        btTransform transform;
        btVector3 t_origin;
        obj.rigid_body->getMotionState()->getWorldTransform(transform); //.getOrigin();
        t_origin = transform.getOrigin();
        obj.transform.translation = {
            t_origin.x(), t_origin.y(), t_origin.z()
        };
        btQuaternion bt_rot = transform.getRotation();
        obj.transform.rotation.x = bt_rot.x();
        obj.transform.rotation.y = bt_rot.y();
        obj.transform.rotation.z = bt_rot.z();
        obj.transform.rotation.w = bt_rot.w();
    }

public:
    BulletWrapper() {
        collision_conf = new btDefaultCollisionConfiguration();
        dispatcher = new btCollisionDispatcher(collision_conf);
        broadphase = new btDbvtBroadphase();
        solver = new btSequentialImpulseConstraintSolver();

        dynamics_world = new btDiscreteDynamicsWorld(
            dispatcher, broadphase, solver, collision_conf
        );

        dynamics_world->setGravity(btVector3(0, 9.81, 0));
    }
    ~BulletWrapper() {
        delete dynamics_world;
        delete solver;
        delete broadphase;
        delete dispatcher;
        delete collision_conf;
    }

    // "resource acquisition is initialization"
    BulletWrapper(const BulletWrapper &) = delete;
    BulletWrapper& operator=(const BulletWrapper &) = delete;
    BulletWrapper(BulletWrapper &&) = delete;
    BulletWrapper& operator=(BulletWrapper &&) = delete;

    /**
    * Steps the physics simulation by one frame.
    *
    * @param objects a vector of EngineObjectQuat objects currently present
    *        in the scene
    * @param dt the delta time of the current frame
    */
    void step(std::vector<EngineObjectQuat> &objects, float dt) {
        dynamics_world->stepSimulation(dt, 10);
        for (auto &obj: objects) {
            if (obj.rigid_body != nullptr) {
                step_obj(obj);
            }
        }
    }

    /**
    * Retrieves the name of an EngineObjectQuat given a collision object
    */
    std::string get_object_name_with_collision_object(
            VDict<EngineObjectQuat> &objects,
            const btCollisionObject* collision_obj
    ) {
        for (auto &obj: objects.values()) {
            if (obj.rigid_body && obj.rigid_body == collision_obj) {
                return obj.name;
            }
        }
        std::cerr <<
            "trying to retrieve an object for a collision object that isn't bound"
        << std::endl;
        return "";
    }

    /**
    * Casts a ray from a given position in a given direction and returns the
    * eventual intersecting EngineObjectQuat 's name
    */
    std::string ray_test(glm::vec3 &source_pos, glm::vec3 direction, VDict<EngineObjectQuat> &objects) {
        btVector3 start{
            source_pos.x,
            source_pos.y,
            source_pos.z
        };
        glm::vec3 gend = (direction * 1.0f);
        btVector3 end = start + btVector3{gend.x, gend.y, gend.z};
        btCollisionWorld::ClosestRayResultCallback res(start, end);
        dynamics_world->getCollisionWorld()->rayTest(start, end, res);
        if (res.hasHit()) {
            if (res.m_collisionObject->isStaticObject()) return "";
            return get_object_name_with_collision_object(
                objects, res.m_collisionObject
            );
        }
        return "";
    }

    /**
    * Given an EngineObjectQuat and its rough collider shape, creates a rigid
    * body that matches its shape and size for use in the physics simulation
    *
    * @param fixed indicates that the object interacts with the physics world
    *        but it's fixed and immobile in space
    */
    void create_rigid_body(
            EngineObjectQuat &obj,
            ColliderShape collider_type,
            int collision_flags=0,
            bool fixed=false
    ) {
        if (fixed) {
            collision_flags |=
                btCollisionObject::CollisionFlags::CF_STATIC_OBJECT;
        }
        else {
            collision_flags |=
                btCollisionObject::CollisionFlags::CF_DYNAMIC_OBJECT;
        }
        glm::vec3 bounding_box_size = obj.get_bounding_box_size();
        btCollisionShape* collision_shape;
        auto half_extents = btVector3(
            bounding_box_size.x, bounding_box_size.y, bounding_box_size.z
        );
        switch (collider_type) {
            case BOX:
                collision_shape = new btBoxShape(half_extents);
                break;
            case CYLINDER:
                collision_shape = new btCylinderShape(half_extents);
                break;
            case SPHERE:
                throw std::runtime_error("sphere collider shape not implemented");
                break;
            case CAPSULE:
                collision_shape = new btCapsuleShape(
                    half_extents.x()*2.0f, half_extents.y()*2.0f
                );
                break;
            default:
                throw std::runtime_error("unknown collider shape");
        }
        btDefaultMotionState* motion_state = new btDefaultMotionState(btTransform(
            btQuaternion(
                obj.transform.rotation.x,
                obj.transform.rotation.y,
                obj.transform.rotation.z,
                obj.transform.rotation.w
            ),
            btVector3(
                obj.transform.translation.x,
                obj.transform.translation.y,
                obj.transform.translation.z
            )
        ));

        btScalar mass = fixed ? 0 : 10;
        auto inertia = btVector3(0, 0, 0);
        if (!fixed) collision_shape->calculateLocalInertia(mass, inertia);
        btRigidBody::btRigidBodyConstructionInfo rigid_body_info(
            mass,  // mass in kg
            motion_state,
            collision_shape,
            inertia
        );
        rigid_body_info.m_friction = 0.9;
        rigid_body_info.m_restitution = 0.2;

        btRigidBody* rigid_body = new btRigidBody(rigid_body_info);
        // broken, raii and references don't go well with regular pointers
        // rigid_body->setUserPointer(&obj);  // so that we can retrieve the object from the rigid body
        rigid_body->setCollisionFlags(collision_flags);
        dynamics_world->addRigidBody(rigid_body);
        obj.rigid_body = rigid_body;
    }

    /**
    * Given an EngineObjectQuat, if it has a rigid body, removes it from the
    * simulation and deletes it
    */
    void remove_rigid_body(EngineObjectQuat &obj) {
        if (obj.rigid_body) {
            dynamics_world->removeRigidBody(obj.rigid_body);
            delete obj.rigid_body;
            obj.rigid_body = nullptr;
        }
    }
};
