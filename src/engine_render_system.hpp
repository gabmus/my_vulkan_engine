#pragma once

#include "defines.hpp"
#include "engine_pipeline.hpp"
#include "engine_device.hpp"
#include "engine_object_quat.hpp"
#include "engine_camera.hpp"
#include "engine_descriptors.hpp"

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <array>
#include <memory>
#include <stdexcept>
#include <utility>
#include <vector>
#include <vulkan/vulkan_core.h>

struct SimplePushConstantData {
    glm::mat4 transform{1.f};
    glm::vec4 color;
    glm::vec4 camera_pos;
    glm::vec4 has_normalmap{0.0f};
};

/**
* Standard render system used for rendering EngineObjectQuat objects
*/
class EngineRenderSystem {
private:
    EngineDevice &device;
    EngineDescriptorManager &descman;
    std::unique_ptr<EnginePipeline> pipeline;
    VkPipelineLayout pipeline_layout;

    void create_pipeline_layout() {
        VkPushConstantRange push_constant_range{};
        push_constant_range.stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT;
        push_constant_range.offset = 0;
        push_constant_range.size = sizeof(SimplePushConstantData);

        VkPipelineLayoutCreateInfo pipeline_layout_info{};
        pipeline_layout_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;

        std::vector<VkDescriptorSetLayout> descriptor_sets_layouts{};
        descman.get_regular_layouts(descriptor_sets_layouts);
        pipeline_layout_info.setLayoutCount = descriptor_sets_layouts.size();
        pipeline_layout_info.pSetLayouts = descriptor_sets_layouts.data();

        pipeline_layout_info.pushConstantRangeCount = 1;
        pipeline_layout_info.pPushConstantRanges = &push_constant_range;

        if (vkCreatePipelineLayout(
                device.device(), &pipeline_layout_info, nullptr, &pipeline_layout
        ) != VK_SUCCESS) {
            throw std::runtime_error("failed to create pipeline layout");
        }
    }

    //! [Creating a regular Pipeline]
    void create_pipeline(VkRenderPass render_pass) {
        assert(pipeline_layout != nullptr && "Cannot create pipeline before pipeline layout");

        PipelineConfigInfo pipeline_config{};
        EnginePipeline::default_pipeline_config_info(pipeline_config);
        pipeline_config.render_pass = render_pass;
        pipeline_config.pipeline_layout = pipeline_layout;
        std::vector<PipelineShaderData> shaders_data = {
            {
                SHADERS_DIR+"/diffuse.vert.spv",
                VK_SHADER_STAGE_VERTEX_BIT
            },
            {
                SHADERS_DIR+"/diffuse.geom.spv",
                VK_SHADER_STAGE_GEOMETRY_BIT
            },
            {
                SHADERS_DIR+"/diffuse.frag.spv",
                VK_SHADER_STAGE_FRAGMENT_BIT
            }
        };
        pipeline = std::make_unique<EnginePipeline>(
            device,
            shaders_data,
            pipeline_config
        );
    }
    //! [Creating a regular Pipeline]

public:
    EngineRenderSystem(
            EngineDevice &device, EngineDescriptorManager &descman, VkRenderPass render_pass
    ) : device{device}, descman{descman} {
        create_pipeline_layout();
        create_pipeline(render_pass);
    }

    ~EngineRenderSystem() {
        vkDestroyPipelineLayout(device.device(), pipeline_layout, nullptr);
    }

    // RAII
    EngineRenderSystem(const EngineRenderSystem &) = delete;
    EngineRenderSystem& operator=(const EngineRenderSystem &) = delete;

    void render_engine_objects(
            VkCommandBuffer command_buffer,
            std::vector<EngineObjectQuat> &objects,
            uint32_t frame_index
    ) {
        SimplePushConstantData push{};
        render_engine_objects(command_buffer, objects, push, frame_index);
    }

    void render_engine_objects(
            VkCommandBuffer command_buffer,
            std::vector<EngineObjectQuat> &objects,
            SimplePushConstantData &push,
            uint32_t frame_index
    ) {
        pipeline->bind(command_buffer);
        // TODO: bind descriptorsets only if pipeline changed?
        // amend: different pipelines are now used per render pass, therefore
        //        this should always happen?
        descman.bind_descriptor_set(command_buffer, pipeline_layout, frame_index);

        for (auto &obj: objects) {
            descman.bind_texture(command_buffer, pipeline_layout, obj);

            push.color = glm::vec4(obj.color, 1.0f);
            push.transform = obj.transform.mat4();
            push.has_normalmap = glm::vec4(obj.model->has_normalmap(), 0.0f, 0.0f, 0.0f);

            vkCmdPushConstants(
                command_buffer, pipeline_layout,
                VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
                0, sizeof(SimplePushConstantData), &push
            );
            obj.model->bind(command_buffer);
            obj.model->draw(command_buffer);
        }
    }
};
