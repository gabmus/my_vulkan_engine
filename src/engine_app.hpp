#pragma once

#include "defines.hpp"
#include "engine_window.hpp"
#include "engine_device.hpp"
#include "engine_object.hpp"
#include "engine_object_quat.hpp"
#include "engine_renderer.hpp"
#include "offscreen.hpp"
#include "engine_render_system.hpp"
#include "skybox_render_system.hpp"
#include "lights_render_system.hpp"
#include "offscreen_render_system.hpp"
#include "fx_manager.hpp"
#include "engine_camera.hpp"
#include "keyboard_movement_controller.hpp"
#include "imgui_middleware.hpp"
#include "bullet_wrapper.hpp"
#include "vector_dict.hpp"
#include <glm/gtc/quaternion.hpp>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <array>
#include <memory>
#include <stdexcept>
#include <utility>
#include <vector>
#include <vulkan/vulkan_core.h>
#include <chrono>
#include <unordered_map>

/**
* Main class and entry point of the application
*/
class EngineApp {
private:
    static constexpr float MAX_FRAME_TIME = 1.f;

    // I guess this means "create this variable this way on class instantiation"
    EngineWindow window{APP_ID};
    EngineDevice device{window};
    EngineRenderer renderer{window, device};
    BulletWrapper bullet_wrapper{};
    ConfMan &confman = ConfMan::get_instance();

    VDict<EngineObjectQuat> objects{};
    
    const glm::vec3 light_pos{1.f, -3.f, -2.f};
    const glm::vec3 light_color{1.f, 1.f, 1.f};

    /**
    * Load the EngineObjectQuat object and their relative EngineModel objects
    * for use in the scene.
    */
    void load_engine_objects() {
        std::shared_ptr<EngineModel> barrel_model = EngineModel::new_model_from_obj(
            device, ASSETS_DIR+"/assets/mbarrel/barrel.obj", ASSETS_DIR+"/assets/mbarrel"
        );
        auto barrel_obj = EngineObjectQuat::create_engine_object();
        barrel_obj.model = barrel_model;
        barrel_obj.transform.translation = {.0f, -1.0f, .0f};
        barrel_obj.transform.scale = {.5f, .5f, .5f};
        barrel_obj.name = "barrel";
        bullet_wrapper.create_rigid_body(barrel_obj, ColliderShape::CYLINDER);
        objects.insert("barrel", std::move(barrel_obj));

        std::shared_ptr<EngineModel> crate_model = EngineModel::new_model_from_obj(
            device, ASSETS_DIR+"/assets/crate/crate.obj", ASSETS_DIR+"/assets/crate"
        );
        auto crate_obj = EngineObjectQuat::create_engine_object();
        crate_obj.model = crate_model;
        crate_obj.transform.translation = {-1.f, .0f, .0f};
        crate_obj.transform.scale = glm::vec3{.3f};
        crate_obj.name = "crate";
        bullet_wrapper.create_rigid_body(crate_obj, ColliderShape::BOX);
        objects.insert("crate", std::move(crate_obj));

        std::shared_ptr<EngineModel> floor_model = EngineModel::new_model_from_obj(
            device, ASSETS_DIR+"/assets/stone_floor/stone_floor.obj",
            ASSETS_DIR+"/assets/stone_floor"
        );
        auto floor_obj = EngineObjectQuat::create_engine_object();
        floor_obj.model = floor_model;
        floor_obj.transform.translation = {0.f, .5f, .0f};
        floor_obj.transform.scale = glm::vec3{.2f};
        floor_obj.name = "floor";
        bullet_wrapper.create_rigid_body(floor_obj, ColliderShape::BOX, 0, true);
        objects.insert("floor", std::move(floor_obj));

        std::shared_ptr<EngineModel> wall0_model = EngineModel::new_model_from_obj(
            device, ASSETS_DIR+"/assets/stone_floor/wall0.obj",
            ASSETS_DIR+"/assets/stone_floor"
        );
        std::shared_ptr<EngineModel> wall1_model = EngineModel::new_model_from_obj(
            device, ASSETS_DIR+"/assets/stone_floor/wall1.obj",
            ASSETS_DIR+"/assets/stone_floor"
        );
        auto wall0_obj = EngineObjectQuat::create_engine_object();
        wall0_obj.model = wall1_model;
        wall0_obj.transform.translation = {4.f, -.7f, 0.f};
        wall0_obj.transform.scale = glm::vec3{.4f};
        wall0_obj.name = "wall0";
        bullet_wrapper.create_rigid_body(wall0_obj, ColliderShape::BOX, 0, true);
        objects.insert("wall0", std::move(wall0_obj));
        auto wall1_obj = EngineObjectQuat::create_engine_object();
        wall1_obj.model = wall1_model;
        wall1_obj.transform.translation = {-4.f, -.7f, 0.f};
        wall1_obj.transform.scale = glm::vec3{.4f};
        wall1_obj.name = "wall1";
        bullet_wrapper.create_rigid_body(wall1_obj, ColliderShape::BOX, 0, true);
        objects.insert("wall1", std::move(wall1_obj));
        auto wall2_obj = EngineObjectQuat::create_engine_object();
        wall2_obj.model = wall0_model;
        wall2_obj.transform.translation = {0.f, -.7f, 4.f};
        wall2_obj.transform.scale = glm::vec3{.4f};
        wall2_obj.name = "wall2";
        bullet_wrapper.create_rigid_body(wall2_obj, ColliderShape::BOX, 0, true);
        objects.insert("wall2", std::move(wall2_obj));
        auto wall3_obj = EngineObjectQuat::create_engine_object();
        wall3_obj.model = wall0_model;
        wall3_obj.transform.translation = {0.f, -.7f, -4.f};
        wall3_obj.transform.scale = glm::vec3{.4f};
        wall3_obj.name = "wall3";
        bullet_wrapper.create_rigid_body(wall3_obj, ColliderShape::BOX, 0, true);
        objects.insert("wall3", std::move(wall3_obj));
    }

    /**
    * Manages the current EngineObjectQuat objects lifecycle: in case a certain
    * object has a limited life time, this method reduces it each frame and
    * eventually deals with "killing" it, gracefully removing it from the scene
    */
    void tick_objects_lifetime(float dt) {
        bool kill = false;
        for (size_t i=0; i<objects.size(); i++) {
            if (objects[i].can_die) {
                objects[i].lifetime -= dt;
                if (objects[i].lifetime <= 0.0) {
                    bullet_wrapper.remove_rigid_body(objects[i]);
                    objects[i].to_kill = true;
                    kill = true;
                }
            }
        }
        if (kill) {
            vkDeviceWaitIdle(device.device());
            objects.remove_if([](EngineObjectQuat &obj) {
                return obj.to_kill;
            });
        }
    }

public:
    EngineApp() {
        load_engine_objects();
    }
    ~EngineApp() {
    }

    // "resource acquisition is initialization"
    EngineApp(const EngineApp &) = delete;
    EngineApp& operator=(const EngineApp &) = delete;

    /**
    * Launches a physics cube forward from the camera
    */
    void fire_cube(EngineObject &viewer_object, EngineDescriptorManager &descman) {
        std::shared_ptr<EngineModel> cube_model = EngineModel::new_cube(device);
        auto cube_obj = EngineObjectQuat::create_engine_object();
        cube_obj.model = cube_model;
        cube_obj.transform.translation = viewer_object.transform.translation +
                                         (viewer_object.get_direction() * 0.2f);
        cube_obj.transform.scale = glm::vec3(.1f);
        cube_obj.name = "projectile_" + std::to_string(cube_obj.getId());
        std::string key = cube_obj.name;
        cube_obj.can_die = true;
        cube_obj.lifetime = 10.0f;
        descman.set_object_descriptor_set(cube_obj);
        bullet_wrapper.create_rigid_body(cube_obj, ColliderShape::BOX);
        cube_obj.apply_force(viewer_object.get_direction(), 10.0f);
        objects.insert(key, std::move(cube_obj));
    }

    /**
    * Main loop of the scene
    */
    void run() {
        SimplePushConstantData push{};
        EngineCamera camera{};
        // cubemap order is +X, -X, +Y, -Y, +Z, -Z
        std::shared_ptr<TextureData> skybox_texture_data = device.create_cubemap_texture(
            std::array<std::string, 6>{
                ASSETS_DIR+"/assets/calm_sea_skybox/calm_sea_ft.jpg",
                ASSETS_DIR+"/assets/calm_sea_skybox/calm_sea_bk.jpg",
                ASSETS_DIR+"/assets/calm_sea_skybox/calm_sea_up.jpg",
                ASSETS_DIR+"/assets/calm_sea_skybox/calm_sea_dn.jpg",
                ASSETS_DIR+"/assets/calm_sea_skybox/calm_sea_rt.jpg",
                ASSETS_DIR+"/assets/calm_sea_skybox/calm_sea_lf.jpg",
            }
        );
        EngineDescriptorManager descman{
            device, EngineSwapChain::MAX_FRAMES_IN_FLIGHT, objects.values(), skybox_texture_data,
        };
        auto swapchain_extent = renderer.get_swap_chain_data().extent;
        FxManager fxman{
            device, descman, swapchain_extent.width, swapchain_extent.height,
            renderer.get_swap_chain_render_pass()
        };
        Offscreen &scene_offscreen = fxman.scene_offscreen();
        auto scene_offscreen_render_pass = scene_offscreen.get_render_pass();
        EngineRenderSystem render_system{device, descman, scene_offscreen_render_pass};
        SkyboxRenderSystem skybox_render_system{
            device, descman, scene_offscreen_render_pass,
            skybox_texture_data
        };
        LightsRenderSystem lights_render_system{
            device, descman, scene_offscreen_render_pass
        };
        ImguiMiddleware imgui_middleware{window.get_glfw_window(), device, renderer};

        /* HandRenderSystem hand_rs{ */
        /*     device, descman, scene_offscreen.get_render_pass() */
        /* }; */

        // just used to store the camera state
        auto viewer_object = EngineObject();
        viewer_object.transform.translation.z -= 2;
        KeyboardMovementController camera_controller{window.get_glfw_window()};

        auto scene_data = SceneData{};
        scene_data.light_pos = glm::vec4(light_pos, 1.0f);
        scene_data.light_color = glm::vec4(light_color, 10.0f); // w: intensity


        auto current_time = std::chrono::high_resolution_clock::now();

        float fov = confman.get_conf()["graphics"]["fov"];
        float view_distance = confman.get_conf()["graphics"]["view_distance"];

        uint32_t frame_index = 999;

        /* EngineCamera hand_camera{}; */
        /* hand_camera.set_perspective_projection( */
        /*     glm::radians(50.f), renderer.get_aspect_ratio(), .1f, 10.f */
        /* ); */
        camera.set_perspective_projection(
            glm::radians(fov), renderer.get_aspect_ratio(), .1f, view_distance
        );

        KeyboardMovementController::Actions actions_todo;

        bool camera_projection_changed = false;

        while(!window.should_close()) {
            glfwPollEvents();

            auto new_time = std::chrono::high_resolution_clock::now();
            float frame_time = glm::min(std::chrono::duration<
                float, std::chrono::seconds::period
            >(new_time - current_time).count(), MAX_FRAME_TIME);
            current_time = new_time;

            bullet_wrapper.step(objects.values(), frame_time);
            actions_todo = camera_controller.move_in_plane_xz(
                frame_time, viewer_object
            );
            if (actions_todo.fire) {
                fire_cube(viewer_object, descman);
            }
            if (actions_todo.grab) {
                auto target_name = bullet_wrapper.ray_test(
                    viewer_object.transform.translation,
                    viewer_object.get_direction(),
                    objects
                );
                if (!target_name.empty()) {
                    auto &obj = objects[target_name];
                    std::cout << "Grab " << obj.name << std::endl;
                    // TODO: grab
                }
            }
            camera.set_view_yxz(viewer_object.transform.translation, viewer_object.transform.rotation);

            // objects manipulation here
            push.camera_pos = glm::vec4(viewer_object.transform.translation, 1.0f);

            if (renderer.swap_chain_changed()) {
                swapchain_extent = renderer.get_swap_chain_data().extent;
                fxman.recreate_offscreens(swapchain_extent.width, swapchain_extent.height);
                /* hand_camera.set_perspective_projection( */
                /*     glm::radians(50.f), renderer.get_aspect_ratio(), .1f, 10.f */
                /* ); */
                camera_projection_changed = true;
                renderer.reset_swap_chain_changed();
            }

            if (camera_projection_changed) {
                camera.set_perspective_projection(
                    glm::radians(fov), renderer.get_aspect_ratio(), .1f, view_distance
                );
                camera_projection_changed = false;
            }

            if (auto command_buffer = renderer.begin_frame()) {
                auto cam_data = camera.get_cam_data();
                frame_index = renderer.get_frame_index();
                descman.copy_camera_data(cam_data, frame_index);
                descman.copy_scene_data(scene_data, frame_index);
                scene_offscreen.begin_offscreen_render_pass(command_buffer);
                skybox_render_system.render(
                    command_buffer, frame_index
                );
                lights_render_system.render(
                    command_buffer, frame_index
                );
                render_system.render_engine_objects(
                    command_buffer, objects.values(), push, frame_index
                );

                /* hand_rs.render(command_buffer, frame_index, viewer_object, push); */
                
                scene_offscreen.end_offscreen_render_pass(command_buffer);

                fxman.render(command_buffer);

                renderer.begin_swap_chain_render_pass(command_buffer);
                imgui_middleware.begin_draw(
                    objects,
                    scene_data,
                    &fov,
                    &view_distance,
                    &camera_projection_changed,
                    window,
                    camera_controller,
                    fxman,
                    frame_time,
                    !camera_controller.mouse_locked()
                );

                fxman.final_draw(command_buffer);

                imgui_middleware.end_draw(command_buffer);
                renderer.end_swap_chain_render_pass(command_buffer);
                renderer.end_frame();
            }
            tick_objects_lifetime(frame_time);
        }

        vkDeviceWaitIdle(device.device());
    }
};
