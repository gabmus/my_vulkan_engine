#pragma once

#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>

#include <iostream>

/**
* Simple dictionary implementation using unordered map and vector
*/
template<class T> class VDict {
private:
    std::unordered_map<std::string, size_t> keymap{};
    std::vector<T> values_{};

public:

    VDict() {
    }
    ~VDict() {
        keymap.clear();
        values_.clear();
    }

    std::vector<T> &values() { return values_; }

    void insert(std::string key, T value) {
        assert(!has_key(key) && "Cannot overwrite existing key in VDict");
        values_.push_back(std::move(value));
        keymap[key] = values_.size()-1;
    }

    void remove(std::string key) {
        assert(has_key(key) && "Cannot remove item with non existing key in VDict");
        for (auto kvp: keymap) {
            if (kvp.second > keymap[key]) {
                keymap[kvp.first]--;
            }
        }
        values_.erase(values_.begin()+keymap[key]);
        keymap.erase(key);
    }

    void remove(size_t index) {
        for (auto kvp: keymap) {
            if (kvp.second == index) {
                return remove(kvp.first);
            }
        }
    }

    void remove_if(bool(*pred)(T &v)) {
        std::vector<std::string> keys_to_remove{};
        for (auto kvp: keymap) {
            if (pred(values_[kvp.second])) {
                keys_to_remove.push_back(kvp.first);
            }
        }
        for (auto key: keys_to_remove) {
            remove(key);
        }
    }

    bool has_key(std::string key) {
        return keymap.find(key) != keymap.end();
    }

    T &at(std::string key) {
        return values_[keymap[key]];
    }

    T &at(size_t index) {
        return values_[index];
    }

    T &operator[](std::string key) {
        return at(key);
    }

    T &operator[](size_t index) {
        return at(index);
    }


    size_t size() { return values_.size(); }
};
