#pragma once

#include "engine_device.hpp"
#include "attachment.hpp"

// using 8 bit color data introduces dithering
#define OFFSCREEN_COLOR_FORMAT VK_FORMAT_R16G16B16A16_UNORM

/**
* Off screen render target.
*
* This is used for intermediate passes that are required to create the frames
* to be presented
*/
class Offscreen {
private:
    EngineDevice& device;

    uint32_t width, height;
    VkFormat depth_format;

    VkRenderPass render_pass;
    VkFramebuffer framebuffer;

    VkFormat findDepthFormat() {
        return device.find_supported_format(
                {
                    VK_FORMAT_D32_SFLOAT,
                    VK_FORMAT_D32_SFLOAT_S8_UINT,
                    VK_FORMAT_D24_UNORM_S8_UINT
                },
                VK_IMAGE_TILING_OPTIMAL,
                VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
        );
    }

    std::unique_ptr<Attachment> color_attachment;
    std::unique_ptr<Attachment> depth_attachment;

    void prepare() {
        depth_format = findDepthFormat();

        if (color_attachment) {
            color_attachment->set_size(width, height);
        }
        else {
            color_attachment = std::make_unique<Attachment>(
                device, OFFSCREEN_COLOR_FORMAT, width, height,
                VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
                VK_IMAGE_ASPECT_COLOR_BIT,
                VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                0, reduce_size
            );
        }
        if (depth_attachment) {
            depth_attachment->set_size(width, height);
        }
        else {
            depth_attachment = std::make_unique<Attachment>(
                device, depth_format, width, height,
                VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
                VK_IMAGE_ASPECT_DEPTH_BIT,
                VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
                VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
                1, reduce_size
            );
        }

        std::array<VkAttachmentDescription, 2> attachment_descriptions{
            color_attachment->get_description(),
            depth_attachment->get_description()
        };

        VkSubpassDescription subpass_description = {};
        subpass_description.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpass_description.colorAttachmentCount = 1;
        subpass_description.pColorAttachments = &color_attachment->get_reference();
        subpass_description.pDepthStencilAttachment = &depth_attachment->get_reference();

        // Use subpass dependencies for layout transitions
        std::array<VkSubpassDependency, 2> dependencies;

        dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
        dependencies[0].dstSubpass = 0;
        dependencies[0].srcStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
        dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependencies[0].srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
        dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

        dependencies[1].srcSubpass = 0;
        dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
        dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        dependencies[1].dstStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
        dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        dependencies[1].dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
        dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

        // Create the actual renderpass
        VkRenderPassCreateInfo render_pass_info = {};
        render_pass_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        render_pass_info.attachmentCount = static_cast<uint32_t>(attachment_descriptions.size());
        render_pass_info.pAttachments = attachment_descriptions.data();
        render_pass_info.subpassCount = 1;
        render_pass_info.pSubpasses = &subpass_description;
        render_pass_info.dependencyCount = static_cast<uint32_t>(dependencies.size());
        render_pass_info.pDependencies = dependencies.data();

        if (vkCreateRenderPass(
                device.device(), &render_pass_info, nullptr, &render_pass
        ) != VK_SUCCESS) {
            throw std::runtime_error("failed to create offscreen render pass!");
        }

        std::array<VkImageView, 2> attachment_image_views{
            color_attachment->get_image_view(),
            depth_attachment->get_image_view(),
        };

        VkFramebufferCreateInfo fbuf_info{};
        fbuf_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        fbuf_info.renderPass = render_pass;
        fbuf_info.attachmentCount = attachment_image_views.size();
        fbuf_info.pAttachments = attachment_image_views.data();
        fbuf_info.width = width / size_divisor;
        fbuf_info.height = height / size_divisor;
        fbuf_info.layers = 1;

        if (vkCreateFramebuffer(
                device.device(),
                &fbuf_info,
                nullptr,
                &framebuffer
        ) != VK_SUCCESS) {
            throw std::runtime_error("failed to create offscreen framebuffer!");
        }
    }

    void cleanup() {
        vkDestroyFramebuffer(device.device(), framebuffer, nullptr);
        vkDestroyRenderPass(device.device(), render_pass, nullptr);
    }
    
    bool reduce_size{false};
    uint32_t size_divisor{1u};

public:

    VkDescriptorSet descriptor_set;
    bool descriptor_set_initialized = false;

    Offscreen(
            EngineDevice &device,
            uint32_t w, uint32_t h,
            bool reduce_size=false
    ) : device{device}, width{w}, height{h}, reduce_size{reduce_size},
        size_divisor{reduce_size ? 2u : 1u} {
        prepare();
    }
    ~Offscreen() {
        cleanup();
        color_attachment.reset();
        depth_attachment.reset();
    }

    void set_size(uint32_t w, uint32_t h) {
        cleanup();
        width = w; height = h;
        prepare();
    }

    void begin_offscreen_render_pass(VkCommandBuffer command_buffer) {
        std::array<VkClearValue, 2> clear_values;
        // magenta clear color, with the skybox you should really never see it
        clear_values[0].color = {{.1f, 0.f, .1f, 0.f}};
        clear_values[1].depthStencil = {1.0f, 0};

        VkRenderPassBeginInfo render_pass_begin_info{};
        render_pass_begin_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        render_pass_begin_info.renderPass = render_pass;
        render_pass_begin_info.framebuffer = framebuffer;
        render_pass_begin_info.renderArea.offset = {0, 0};
        render_pass_begin_info.renderArea.extent.width = width/size_divisor;
        render_pass_begin_info.renderArea.extent.height = height/size_divisor;
        render_pass_begin_info.clearValueCount = static_cast<uint32_t>(clear_values.size());
        render_pass_begin_info.pClearValues = clear_values.data();

        vkCmdBeginRenderPass(
            command_buffer, &render_pass_begin_info,
            VK_SUBPASS_CONTENTS_INLINE
        );

        VkViewport viewport{};
        viewport.x = 0.f; viewport.y = 0.f;
        viewport.width = static_cast<float>(width / size_divisor);
        viewport.height = static_cast<float>(height / size_divisor);
        viewport.minDepth = 0.0f; viewport.maxDepth = 1.0f;
        VkRect2D scissor{{0, 0}, {width/size_divisor, height/size_divisor}};
        vkCmdSetViewport(command_buffer, 0, 1, &viewport);
        vkCmdSetScissor(command_buffer, 0, 1, &scissor);
    }

    void end_offscreen_render_pass(VkCommandBuffer command_buffer) {
        vkCmdEndRenderPass(command_buffer);
    }

    VkRenderPass &get_render_pass() {
        return render_pass;
    }
    VkImageView &get_image_view() {
        return color_attachment->get_image_view();
    }
    VkSampler &get_sampler() {
        return color_attachment->get_sampler();
    }
    VkImage &get_image() {
        return color_attachment->get_image();
    }

    // RAII
    Offscreen(const Offscreen &) = delete;
    Offscreen& operator=(const Offscreen &) = delete;
    Offscreen(Offscreen &&) = delete;
    Offscreen& operator=(Offscreen &&) = delete;
};
