#pragma once

#include "engine_descriptors.hpp"
#include "engine_device.hpp"
#include "offscreen_render_system.hpp"
#include "combine_render_system.hpp"


/**
* Responsible for rendering post-processing effects (currently bloom and FXAA)
*
* The bloom effect is divided in 3 passes:
*
* - brightness filter: filter out the darker parts of a frame, making them
*   black and keeping everything else untouched
* - blur: blur out the resulting image using the
*   [Kawase blur algorithm](https://community.arm.com/cfs-file/__key/communityserver-blogs-components-weblogfiles/00-00-00-20-66/siggraph2015_2D00_mmg_2D00_marius_2D00_notes.pdf),
*   in two passes, resulting in an effect similar to a comparable gaussian
*   blur, but faster
* - combine: add the color of the fragments from resulting blurred image
*   to the ones in the original image to create the final bloom effect
*/
class FxManager {
private:
    EngineDevice &device;
    EngineDescriptorManager &descman;
    std::array<Offscreen, 4> offscreens;
    ConfMan &confman{ConfMan::get_instance()};

    OffscreenRenderSystem brightness_filter_rs;
    OffscreenRenderSystem blur0_rs;
    OffscreenRenderSystem blur1_rs;
    CombineRenderSystem combine_rs;

    OffscreenRenderSystem fxaa_rs;

    OffscreenRenderSystem dummy_rs;
    OffscreenRenderSystem transfer_rs;

    bool bloom{true}, fxaa{true};
public:

    void move_offscren_data_to_scene(VkCommandBuffer command_buffer, Offscreen &src) {
        // maybe change to blit?
        scene_offscreen().begin_offscreen_render_pass(command_buffer);
        transfer_rs.render(command_buffer, src);
        scene_offscreen().end_offscreen_render_pass(command_buffer);
    }

    void draw_settings() {
        if (ImGui::Checkbox("Bloom", &bloom)) {
            auto n_conf = confman.get_conf();
            n_conf["graphics"]["bloom"] = bloom;
            confman.set_conf(n_conf);
        }
        if (ImGui::Checkbox("FXAA", &fxaa)) {
            auto n_conf = confman.get_conf();
            n_conf["graphics"]["fxaa"] = fxaa;
            confman.set_conf(n_conf);
        }
    }

    void render(
            VkCommandBuffer command_buffer
    ) {
        if (confman.get_conf()["graphics"]["bloom"]) {
            offscreens[0].begin_offscreen_render_pass(command_buffer);
            brightness_filter_rs.render(command_buffer, scene_offscreen());
            offscreens[0].end_offscreen_render_pass(command_buffer);

            offscreens[1].begin_offscreen_render_pass(command_buffer);
            blur0_rs.render(command_buffer, offscreens[0]);
            offscreens[1].end_offscreen_render_pass(command_buffer);

            offscreens[0].begin_offscreen_render_pass(command_buffer);
            blur1_rs.render(command_buffer, offscreens[1]);
            offscreens[0].end_offscreen_render_pass(command_buffer);

            target_offscreen().begin_offscreen_render_pass(command_buffer);
            combine_rs.render(command_buffer, scene_offscreen(), offscreens[0]);
            target_offscreen().end_offscreen_render_pass(command_buffer);

            move_offscren_data_to_scene(command_buffer, target_offscreen());
        }
        if (confman.get_conf()["graphics"]["fxaa"]) {
            target_offscreen().begin_offscreen_render_pass(command_buffer);
            fxaa_rs.render(command_buffer, scene_offscreen());
            target_offscreen().end_offscreen_render_pass(command_buffer);

            move_offscren_data_to_scene(command_buffer, target_offscreen());
        }
    }

    void final_draw(VkCommandBuffer command_buffer) {
        dummy_rs.render(command_buffer, scene_offscreen());
    }

    void recreate_offscreens(uint32_t w, uint32_t h) {
        for (auto &off: offscreens) {
            off.set_size(w, h);
            descman.init_fullscreen_descriptor_set(off);
        }
    }

    Offscreen &scene_offscreen() { return offscreens[2]; }
    Offscreen &target_offscreen() { return offscreens[3]; }

    FxManager(
            EngineDevice &device,
            EngineDescriptorManager &descman,
            uint32_t width, uint32_t height,
            VkRenderPass final_render_pass
    ) : device{device}, descman{descman},
        offscreens{
            Offscreen{device, width, height, true},
            Offscreen{device, width, height, true},
            Offscreen{device, width, height},
            Offscreen{device, width, height}
        },
        brightness_filter_rs{OffscreenRenderSystem{
            device, descman, offscreens[0].get_render_pass(),
            SHADERS_DIR+"/brightness_filter.frag.spv"
        }},
        blur0_rs{OffscreenRenderSystem{
            device, descman, offscreens[1].get_render_pass(),
            SHADERS_DIR+"/kawase_down.frag.spv"
        }},
        blur1_rs{OffscreenRenderSystem{
            device, descman, offscreens[0].get_render_pass(),
            SHADERS_DIR+"/kawase_up.frag.spv"
        }},
        combine_rs{CombineRenderSystem{
            device, descman, target_offscreen().get_render_pass()
        }},
        fxaa_rs{OffscreenRenderSystem{
            device, descman, target_offscreen().get_render_pass(),
            SHADERS_DIR+"/fxaa.frag.spv"
        }},
        dummy_rs{OffscreenRenderSystem{
            device, descman, final_render_pass,
            SHADERS_DIR+"/fullscreen.frag.spv"
        }},
        transfer_rs{OffscreenRenderSystem{
            device, descman, scene_offscreen().get_render_pass(),
            SHADERS_DIR+"/fullscreen.frag.spv"
        }}
    {
        for (auto &off: offscreens) {
            descman.init_fullscreen_descriptor_set(off);
        }
    }

    ~FxManager() {}

    // RAII
    FxManager(const FxManager &) = delete;
    FxManager& operator=(const FxManager &) = delete;
    FxManager(FxManager &&) = delete;
    FxManager& operator=(FxManager &&) = delete;
};
