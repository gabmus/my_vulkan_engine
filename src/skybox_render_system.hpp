#pragma once

#include "engine_descriptors.hpp"
#include "engine_pipeline.hpp"

#include <memory>

/**
* Render system used to draw the skybox
*/
class SkyboxRenderSystem {
private:
    EngineDevice &device;
    EngineDescriptorManager &descman;
    std::unique_ptr<EnginePipeline> pipeline;
    VkPipelineLayout pipeline_layout;
    std::shared_ptr<TextureData> skybox_texture_data;
    std::unique_ptr<EngineModel> skybox_model;

    void create_pipeline_layout() {
        VkPipelineLayoutCreateInfo pipeline_layout_info{};
        pipeline_layout_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        std::vector<VkDescriptorSetLayout> descriptor_sets_layouts{};
        descman.get_skybox_layouts(descriptor_sets_layouts);
        pipeline_layout_info.setLayoutCount = descriptor_sets_layouts.size();
        pipeline_layout_info.pSetLayouts = descriptor_sets_layouts.data();
        pipeline_layout_info.pushConstantRangeCount = 0;
        pipeline_layout_info.pPushConstantRanges = nullptr;

        if (vkCreatePipelineLayout(
                device.device(), &pipeline_layout_info, nullptr, &pipeline_layout
        ) != VK_SUCCESS) {
            throw std::runtime_error("failed to create pipeline layout");
        }
    }

    void create_pipeline(VkRenderPass render_pass) {
        assert(pipeline_layout != nullptr && "Cannot create pipeline before pipeline layout");

        PipelineConfigInfo pipeline_config{};
        EnginePipeline::default_pipeline_config_info(pipeline_config);
        pipeline_config.render_pass = render_pass;
        pipeline_config.pipeline_layout = pipeline_layout;
        pipeline_config.rasterization_info.cullMode = VK_CULL_MODE_FRONT_BIT;
        pipeline_config.rasterization_info.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
        pipeline_config.depth_stencil_info.depthTestEnable = VK_FALSE;
        std::vector<PipelineShaderData> shaders_data = {
            {
                SHADERS_DIR+"/skybox.vert.spv",
                VK_SHADER_STAGE_VERTEX_BIT
            },
            {
                SHADERS_DIR+"/skybox.frag.spv",
                VK_SHADER_STAGE_FRAGMENT_BIT
            }
        };
        pipeline = std::make_unique<EnginePipeline>(
            device,
            shaders_data,
            pipeline_config
        );
    }

    void create_skybox_model() {
        skybox_model = EngineModel::new_cube(device);
    }

public:
    SkyboxRenderSystem(
            EngineDevice &device,
            EngineDescriptorManager &descman,
            VkRenderPass render_pass,
            std::shared_ptr<TextureData> skybox_texture_data
    ) : device{device}, descman{descman}, skybox_texture_data{skybox_texture_data} {
        create_pipeline_layout();
        create_pipeline(render_pass);
        create_skybox_model();
    }

    ~SkyboxRenderSystem() {
        skybox_texture_data.reset();
        vkDestroyPipelineLayout(device.device(), pipeline_layout, nullptr);
    }

    // RAII
    SkyboxRenderSystem(const SkyboxRenderSystem &) = delete;
    SkyboxRenderSystem& operator=(const SkyboxRenderSystem &) = delete;

    void render(
            VkCommandBuffer command_buffer, uint32_t frame_index
    ) {
        pipeline->bind(command_buffer);
        descman.bind_skybox_descriptor_set(command_buffer, pipeline_layout, frame_index);
        skybox_model->bind(command_buffer);
        skybox_model->draw(command_buffer);
    }
};
