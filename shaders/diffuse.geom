#version 450

layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

layout (location = 0) out vec3 fragColor;
layout (location = 1) out vec3 normal;
layout (location = 2) out vec3 position;
layout (location = 3) out vec3 lightPos;
layout (location = 4) out vec2 textureCoord;
layout (location = 5) out mat3 fragTBN;

layout (location = 0) in DATA {
    vec3 color;
    vec3 normal;
    vec3 lightPos;
    vec2 textureCoord;
    mat4 viewproj;
    mat4 model_transform;
    float has_normalmap;
} data_in[];

// matrices are lists of columns, not rows identity only just happens to be the same
const mat3 IDENTITY = {
    {1.0f, 0.0f, 0.0f},
    {0.0f, 1.0f, 0.0f},
    {0.0f, 0.0f, 1.0f}
};

void main() {

    mat3 TBN = IDENTITY;
    if (data_in[0].has_normalmap > 0.0f) {
        vec3 edge0 = gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz;
        vec3 edge1 = gl_in[2].gl_Position.xyz - gl_in[0].gl_Position.xyz;
        vec2 deltaUV0 = data_in[1].textureCoord - data_in[0].textureCoord;
        vec2 deltaUV1 = data_in[2].textureCoord - data_in[0].textureCoord;

        float invDet = 1.0f / (deltaUV0.x * deltaUV1.y - deltaUV1.x * deltaUV0.y);

        vec3 tangent = vec3(invDet * (deltaUV1.y * edge0 - deltaUV0.y * edge1));
        vec3 bitangent = vec3(invDet * (deltaUV1.x * edge0 - deltaUV0.x * edge1));

        vec3 T = normalize(-tangent);
        vec3 B = normalize(-bitangent);
        vec3 N = normalize(cross(edge0, edge1));

        TBN = mat3(T, B, N);
        TBN = transpose(TBN);
        /* TBN = transpose(mat3( */
        /*     N, */
        /*     B, */
        /*     T */
        /* )); */
    }


    fragColor = data_in[0].color;
    normal = TBN * data_in[0].normal;
    position = TBN * gl_in[0].gl_Position.xyz;
    lightPos = TBN * data_in[0].lightPos;
    textureCoord = data_in[0].textureCoord;
    fragTBN = TBN;
    gl_Position = data_in[0].viewproj * gl_in[0].gl_Position;
    EmitVertex();

    fragColor = data_in[1].color;
    normal = TBN * data_in[1].normal;
    position = TBN * gl_in[1].gl_Position.xyz;
    lightPos = TBN * data_in[1].lightPos;
    textureCoord = data_in[1].textureCoord;
    fragTBN = TBN;
    gl_Position = data_in[1].viewproj * gl_in[1].gl_Position;
    EmitVertex();

    fragColor = data_in[2].color;
    normal = TBN * data_in[2].normal;
    position = TBN * gl_in[2].gl_Position.xyz;
    lightPos = TBN * data_in[2].lightPos;
    textureCoord = data_in[2].textureCoord;
    fragTBN = TBN;
    gl_Position = data_in[2].viewproj * gl_in[2].gl_Position;
    EmitVertex();

    EndPrimitive();
}
