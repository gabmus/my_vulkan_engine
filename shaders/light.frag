#version 450

layout (location = 0) out vec4 outColor;

layout (set = 0, binding = 1) uniform SceneData {
    vec4 ambient_color;
    vec4 light_color;
    vec4 light_pos;
} scene_data;


void main() {
    outColor = vec4(scene_data.light_color.xyz, 1.0);
}
