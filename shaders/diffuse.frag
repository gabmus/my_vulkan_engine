#version 450

layout (location = 0) in vec3 fragColor;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 position;
layout (location = 3) in vec3 lightPos;
layout (location = 4) in vec2 textureCoord;
layout (location = 5) in mat3 TBN;

layout (location = 0) out vec4 outColor;

layout(push_constant) uniform Push {
    mat4 transform;
    vec4 color;
    vec4 cameraPos;
    vec4 has_normalmap;
} push;

layout (set = 0, binding = 1) uniform SceneData {
    vec4 ambient_color;
    vec4 light_color;
    vec4 light_pos;
} scene_data;
layout (set = 1, binding = 0) uniform sampler2D textureSampler;
layout (set = 1, binding = 1) uniform sampler2D normalmapSampler;

void main() {
    vec3 norm_normal = normalize(
        push.has_normalmap.x > 0.0f ?
            texture(normalmapSampler, textureCoord).xyz * 2.0f -1.0f
            : normal
    );

    // vec3 norm_normal = normalize(normal);
    vec3 unnorm_light_direction = lightPos - position;
    vec3 light_direction = normalize(unnorm_light_direction);

    vec3 color = texture(textureSampler, textureCoord).xyz;
    float attenuation = 1.0 / dot(unnorm_light_direction, unnorm_light_direction);
    float diffuse_amount = max(dot(norm_normal, light_direction), 0.0f);
    vec3 diffuse = diffuse_amount
                   * (scene_data.light_color.xyz * scene_data.light_color.w)
                   * color * attenuation;
    vec3 ambient = (scene_data.ambient_color.xyz * scene_data.ambient_color.w) * color;

    vec3 specular = vec3(0.0f, 0.0f, 0.0f);
    if (diffuse_amount > 0.0f) {
        float specularLight = 0.5f;
        vec3 viewDirection = normalize(TBN * push.cameraPos.xyz - position);
        vec3 reflectionDirection = reflect(-light_direction, norm_normal);
        vec3 halfwayVector = normalize(viewDirection + light_direction);
        float specAmount = pow(max(dot(norm_normal, halfwayVector), 0.0f), 16);
        specular = (specAmount * specularLight) * color * attenuation;
    }
    
    
    // vec3 specular = vec3(0.0f, 0.0f, 0.0f);

    outColor = vec4(ambient+diffuse+specular, 1.0f);
    // outColor = vec4(fragColor, 1.0) * vec4(scene_data.light_color.xyz * diffuse, 1.0);
}
