#version 450

layout (location = 0) in vec2 inUV;

layout (location = 0) out vec4 outColor;

layout (binding = 0) uniform sampler2D fbTexture;

const float THRESHOLD = 0.7f;
const vec4 BLACK = {0.0f, 0.0f, 0.0f, 1.0f};

void main() {
    vec4 color = texture(fbTexture, inUV);
    // this is one way, simple, hard cutoff
    // outColor =
    //     ((color.x * 0.2126f) + (color.y * 0.7152f) + (color.z * 0.0722f)) // get luma
    //     > THRESHOLD ?
    //     color : BLACK
    // ;
    // another way is this, multiply by brightness further times to increase the contrast
    float brightness = ((color.x * 0.2126f) + (color.y * 0.7152f) + (color.z * 0.0722f));
    outColor = color * brightness * brightness * brightness;  // * brightness * brightness...
}
