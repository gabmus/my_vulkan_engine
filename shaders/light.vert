#version 450

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec3 color;
layout(location = 3) in vec2 texCoord;

layout(set = 0, binding = 0) uniform CameraBuffer{
    mat4 view;
    mat4 proj;
    mat4 viewproj;
} cameraData;
layout (set = 0, binding = 1) uniform SceneData {
    vec4 ambient_color;
    vec4 light_color;
    vec4 lightPos;
} scene_data;

void main() {
    mat4 model_transform = {
        {0.2, 0.0, 0.0, 0.0},
        {0.0, 0.2, 0.0, 0.0},
        {0.0, 0.0, 0.2, 0.0},
        {0.0, 0.0, 0.0, 1.0}
    };
    model_transform[3] = vec4(scene_data.lightPos.xyz, 1.0);
    gl_Position = cameraData.viewproj * model_transform * vec4(position, 1.0);
}
