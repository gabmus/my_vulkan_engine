#version 450

layout (location = 0) in vec2 inUV;

layout (location = 0) out vec4 outColor;

layout (binding = 0) uniform sampler2D fbTexture;

const float WEIGHT[5] = {
    0.227027,
    0.1945946,
    0.1216216,
    0.054054,
    0.016216
};

const float SCALE = 1.3f;
const float STRENGTH = 1.5f;

void main() {
    vec2 tex_offset = 1.0f / textureSize(fbTexture, 0) * SCALE;
    vec3 res = texture(fbTexture, inUV).xyz * WEIGHT[0];
    for (int i = 1; i < 5; i++) {
        // H
        res +=
            texture(fbTexture, inUV + vec2(tex_offset.x * i, 0.0)).xyz *
            WEIGHT[i] * STRENGTH;
        res +=
            texture(fbTexture, inUV - vec2(tex_offset.x * i, 0.0)).xyz *
            WEIGHT[i] * STRENGTH;
        // V
        // res +=
        //     texture(fbTexture, inUV + vec2(0.0f, tex_offset.y * i)).xyz *
        //     WEIGHT[i] * STRENGTH;
        // res +=
        //     texture(fbTexture, inUV - vec2(0.0f, tex_offset.y * i)).xyz *
        //     WEIGHT[i] * STRENGTH;
    }

    outColor = vec4(res, 1.0);
}
