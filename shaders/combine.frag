#version 450

layout (location = 0) in vec2 inUV;

layout (location = 0) out vec4 outColor;

layout (set = 0, binding = 0) uniform sampler2D ogTexture;
layout (set = 1, binding = 0) uniform sampler2D fxTexture;

const float INTENSITY = 1.0f;

const float GAMMA = 2.2;
const float INV_GAMMA = 1. / GAMMA;

vec4 LINEARtoSRGB(vec4 color) {
    vec3 res = pow(color.xyz, vec3(INV_GAMMA));
    return vec4(res, color.w);
}

vec4 SRGBtoLINEAR(vec4 srgbIn)
{
    return vec4(pow(srgbIn.xyz, vec3(GAMMA)), srgbIn.w);
}

void main() {
    vec4 color = SRGBtoLINEAR(texture(ogTexture, inUV));
    vec4 fxcolor = SRGBtoLINEAR(texture(fxTexture, inUV));
    color += fxcolor;
    outColor = LINEARtoSRGB(vec4(1.0) - exp(-color * INTENSITY));
}
