#version 450

layout (location = 0) in vec2 inUV;

layout (location = 0) out vec4 outColor;

layout (binding = 0) uniform sampler2D fbTexture;

const float OFFSET = 3;

void main() {
    vec2 resolution = textureSize(fbTexture, 0);
    vec2 halfpixel = 0.5 / resolution;

    vec4 sum = texture(fbTexture, inUV + vec2(-halfpixel.x * 2.0, 0.0) * OFFSET);
    sum += texture(fbTexture, inUV + vec2(-halfpixel.x, halfpixel.y) * OFFSET) * 2.0;
    sum += texture(fbTexture, inUV + vec2(0.0, halfpixel.y * 2.0) * OFFSET);
    sum += texture(fbTexture, inUV + vec2(halfpixel.x, halfpixel.y) * OFFSET) * 2.0;
    sum += texture(fbTexture, inUV + vec2(halfpixel.x * 2.0, 0.0) * OFFSET);
    sum += texture(fbTexture, inUV + vec2(halfpixel.x, -halfpixel.y) * OFFSET) * 2.0;
    sum += texture(fbTexture, inUV + vec2(0.0, -halfpixel.y * 2.0) * OFFSET);
    sum += texture(fbTexture, inUV + vec2(-halfpixel.x, -halfpixel.y) * OFFSET) * 2.0;

    outColor = sum / 12.0;
}
