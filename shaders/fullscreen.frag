#version 450

layout (location = 0) in vec2 inUV;

layout (location = 0) out vec4 outColor;

layout (binding = 0) uniform sampler2D fbTexture;

void main() {
    outColor = texture(fbTexture, inUV);
}
