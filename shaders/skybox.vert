#version 450

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec3 color;
layout(location = 3) in vec2 texCoord;

layout(location = 0) out vec3 outUVW;

layout(set = 0, binding = 0) uniform CameraBuffer{
    mat4 view;
    mat4 proj;
    mat4 viewproj;
} cameraData;

void main() {
    outUVW = position;
    outUVW.xy *= -1.0;
    mat4 view_t = cameraData.view;
    view_t[3] = vec4(0, 0, 0, 1);
    gl_Position =  cameraData.proj * view_t * vec4(position, 1.0);
}
