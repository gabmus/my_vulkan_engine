#version 450

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec3 color;
layout(location = 3) in vec2 texCoord;

layout (location = 0) out DATA {
    vec3 color;
    vec3 normal;
    vec3 lightPos;
    vec2 textureCoord;
    mat4 viewproj;
    mat4 model_transform;
    float has_normalmap;
} data_out;

layout(set = 0, binding = 0) uniform CameraBuffer{
    mat4 view;
    mat4 proj;
    mat4 viewproj;
} cameraData;
layout (set = 0, binding = 1) uniform SceneData {
    vec4 ambient_color;
    vec4 light_color;
    vec4 lightPos;
} scene_data;

layout(push_constant) uniform Push {
    mat4 transform;
    vec4 color;
    vec4 cameraPos;
    vec4 has_normalmap;
} push;

// called once for each vertex
void main() {
    gl_Position = push.transform * vec4(position, 1.0f);
    data_out.color = color;
    data_out.normal = mat3(push.transform) * normal.xyz;// (push.transform * vec4(normal, 1.0f)).xyz;
    data_out.lightPos = scene_data.lightPos.xyz;
    data_out.textureCoord = texCoord;
    // maybe you want to do data_out.textureCoord = mat2(0.0, -1.0, 1.0, 0.0) * texCoord, or it may be an opengl thing
    data_out.viewproj = cameraData.viewproj;
    data_out.model_transform = push.transform;
    data_out.has_normalmap = push.has_normalmap.x > 0.0f ? 1.0f : 0.0f;
//    mat4 transformMatrix = cameraData.viewproj * push.transform;
//    vec4 n_position = transformMatrix * vec4(position, 1.0f);
//    /* vec4 n_normal = transformMatrix * vec4(normal, 1.0f); */
//    vec3 n_normal = mat3(cameraData.view * push.transform) * normal;
//    gl_Position = n_position;
//
//    /* gl_Position = push.transform * vec4(position, 1.0); */
//    fragColor = color;
//    fragNormal = n_normal;
//    fragPosition = n_position.xyz;
//    textureCoord = texCoord;
//    lightPos = (cameraData.viewproj * vec4(scene_data.light_pos.xyz, 1.0)).xyz;
}
