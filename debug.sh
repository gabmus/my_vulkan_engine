#!/bin/bash

set -e

if [[ ! -d build_debug ]]; then
    mkdir build_debug
    pushd build_debug
    meson ..
    meson configure -Dprefix=$PWD/mprefix -Dprofile=debug
    popd
fi

ninja -C build_debug
ninja -C build_debug install
gdb ./build_debug/mprefix/bin/MyFirstVulkanApp
