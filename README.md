# [Vulkan Engine](https://gitlab.com/gabmus/my_vulkan_engine)

[Documentation](https://gabmus.gitlab.io/my_vulkan_engine)

A simple Vulkan engine demo.

- [VMA](https://gpuopen.com/vulkan-memory-allocator/): memory allocation
- [Bullet3](https://github.com/bulletphysics/bullet3): physics engine
- [Dear ImGui](https://github.com/ocornut/imgui): draw the in-game UI and windows
- [tinyobjloader](https://github.com/tinyobjloader/tinyobjloader): load wavefront obj files
- [GLFW](https://www.glfw.org/): manage the windowing and input logic
- [Meson](https://mesonbuild.com/): build system
- [OpenGameArt](opengameart.org/): for the various assets used

This project is written in C++17 and makes extensive use of the [RAII](https://en.cppreference.com/w/cpp/language/raii) technique, along with [smart pointers](https://en.cppreference.com/book/intro/smart_pointers), to facilitate memory management.

Another important aspect to note is that the entire project is written using header files to eliminate the complication of having split headers and implementations, albeit making compilation mostly single-threaded and consequently increasing the compilation time.

---

The scene is comprised of:
- four walls
- a floor
- a barrel
- a crate
- a spherical light source
- a textured skybox

The floor and walls are static physics objects, while the crate and barrel are dynamic and can move around. The main way to interact with these objects is by pressing the left mouse button, which fires a pink cube physics object with a fixed lifetime (meaning they will disappear after a while, this is to avoid having too many objects in the scene). This cube will be able to displace the barrel and/or the crate, as well as other cubes fired previously.

It's possible to press `F` to cast a short ray from the player forward and have the resulting intersecting item printed on the console.

Pressing `Esc` will disable movement and open the main menu, where it's possible to manipulate the barrel and crate, as well as the light. It's also possible to change some parameters like toggling FXAA and bloom, changing the FOV and view distance and calibrating the mouse sensitivity.

The window size can be changed by either resizing the window or using the sliders in the main menu.

The engine works by using the traditional method of forward rendering.

The light model used is the [Blinn-Phong illumination model](https://en.wikipedia.org/wiki/Blinn%E2%80%93Phong_reflection_model), being one of the simpler ones to implement with good results.

As previously stated, the engine is capable of using post-processing effects. There are two effects available:

- FXAA, or [fast approximate anti-aliasing](https://en.wikipedia.org/wiki/Fast_approximate_anti-aliasing), a very fast and decently effective anti-aliasing technique that can be implemented as a post-processing effect (compared to other techniques such as [MSAA](https://en.wikipedia.org/wiki/Multisample_anti-aliasing))
- Bloom, which gives very bright pixels a _halo effect_, contributing to the illusion of them irradiating light of their own
